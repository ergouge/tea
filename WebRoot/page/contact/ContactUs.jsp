<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>联系我们</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="asset/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="asset/css/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js">


/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.5&ak=ND74p1n5sRd97AF6SIStZWRG"></script> 
<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

</script>

<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
  </head>
  
  <body>
    <%@include file="../../header.jsp"%>
    <div id="templatemo_main_wrapper" style="height:1000px;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1; ">
<!--     <div id="templatemo_main"> -->
	<div id="sidebar" class="left">
    	<div class="sidebar_box"><span class="bottom"></span>
            <h3>联系我们</h3>   
            <div class="content"> 
                <ul class="sidebar_list">
                    <li class="actived_li" ><a href="page/contact/ContactUs.jsp"><span>联系方式</span></a></li>
                    <li><a href="page/contact/Recruitment.jsp">人才招聘</a></li>
                    <li><a href="page/contact/JoinIn.jsp">加盟在线申请</a></li>                   
                </ul>
            </div>
        </div>
        <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #000"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                 &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
    </div>
    
    <div id="content" class="right">
		<h2>联系我们</h2>
        <div class="cleaner h20"></div>
        <div class="col col13">
            <h4>通信地址</h4>
        	
        	<br/>
            <strong>地址：</strong>杭州市江干区<br/>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凯旋路70号159#<br/>
          
            
			<strong>电话：</strong>0571-86957651<br />
			<strong>手机：</strong>18958191388<br />
            <strong>传真：</strong>028-86957651<br />
            
            <div class="cleaner h20"></div>
           <%-- <h6><strong>办公地址二</strong></h6>
        	<br/>
            <strong>地址：</strong>杭州市江干区<br/>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;凯旋路70号159#<br/>
          
            
			<strong>电话：</strong>0571-86957651<br />
            <strong>邮箱：</strong> <a href="mailto:contact@company.com">contact@company.com</a> <br />
            
            <h6><strong>办公地址二</strong></h6>
            600-110 Duis lacinia, <br />
            Ullamcorper mattis, 88770<br />
            Maecenas a diam, mollis magna<br /><br />
            
			<strong>Phone:</strong> 020-660-8800<br />
            <strong>Email:</strong> <a href="mailto:info@company.com">info@company.com</a> <br />
            Validate <a href="http://validator.w3.org/check?uri=referer" rel="nofollow"><strong>XHTML</strong></a> &amp; <a href="http://jigsaw.w3.org/css-validator/check/referer" rel="nofollow"><strong>CSS</strong></a>
		--%></div>
        <div class="col col23 no_margin_right">
        	<div class="map_border" style="height: 340px;width: 430px" id="container">
               <%-- <iframe width="430" height="340" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Central+Park,+New+York,+NY,+USA&amp;aq=0&amp;sll=14.093957,1.318359&amp;sspn=69.699334,135.263672&amp;vpsrc=6&amp;ie=UTF8&amp;hq=Central+Park,+New+York,+NY,+USA&amp;ll=40.778265,-73.96988&amp;spn=0.033797,0.06403&amp;t=m&amp;output=embed">
                </iframe>
             --%>
             </div>
			
        </div>
        <div class="cleaner h40"></div>
        <div id="contact_form">
           <form method="post" name="contact" action="#">
           		<div class="col col13">
                
                    <label for="name">姓名:</label> 
              		<input name="name" type="text" class="input_field" id="name" maxlength="40" />
                	<div class="cleaner h10"></div>
                    <label for="email">邮箱:</label> 
          			<input name="email" type="text" class="required input_field" id="email" maxlength="40" />
                	<div class="cleaner h10"></div>
                    <label for="phone">手机:</label> 
        			<input name="phone" type="text" class="input_field" id="phone" maxlength="20" />
                	<div class="cleaner h10"></div>
                	
                
			 	</div>
                
                <div class="col col23 no_margin_right">
                    <label for="message">留言:</label> 
               	  	<textarea id="message" name="message" rows="0" cols="0" class="required"></textarea>
                    <div class="cleaner h10"></div>
                    <input type="submit" class="submit_btn left" name="submit" id="submit" value="发送" />
                    <input type="reset" class="submit_btn submit_right" name="reset" id="reset" value="重置" />
				</div>
                
                
            </form>
        </div>
        <div class="cleaner h40"></div>               
        <div class="cleaner"></div>  
    </div>
    <div class="cleaner"></div>
<!-- </div>  -->
<!-- END of main -->
    </div>
    <%@include file="../../footer.jsp" %>
    	<script type="text/javascript">
var map = new BMap.Map("container");          // 创建地图实例  
var point = new BMap.Point(120.1966490000,30.2565310000);  // 创建点坐标  
map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别 
setTimeout(function(){
		map.setZoom(15);   
	}, 2000);  //2秒后放大到15级
	map.enableScrollWheelZoom(true); 
	var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
	var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
	var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL}); //右
	map.addControl(top_left_control);        
		map.addControl(top_left_navigation);     
		map.addControl(top_right_navigation);    
	
</script> 
  </body>
</html>
