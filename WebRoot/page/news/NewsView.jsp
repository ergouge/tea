<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><c:out value="${news_detail.news_title}" escapeXml="false"></c:out></title>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
<style type="text/css">
a:link {color: #000000; font-family: arial; text-decoration: none}
a:visited {color: #000000; font-family: arial; text-decoration: none}
a:active {color: #FF0000; font-family: arial;text-decoration: none}
a:hover {color: #FF0000; font-family: arial;text-decoration: underline}
body {font-size: 14px;font-family: 宋体;}
th {font-size: 14px;font-family: 宋体;}
td {font-size: 14px;font-family: 宋体;}
</style> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
  </head>
  
  <body>
    <div align="center" >
	<table border="0" width="650" cellspacing="0" cellpadding="0" id="table1">
      <tbody><tr>
        <td height="12"><p align="center"></p></td>
      </tr>
      <tr>
        <td height="45">
        <p align="center"><font size="5" face="黑体"><c:out value="${news_detail.news_title}" escapeXml="false"></c:out></font></p>
        </td>
      </tr>
      <tr>
        <td height="13"><hr></td>
      </tr>
      <tr>
        <td height="25">
        <p align="center"><font color="#666666">类型:<c:out value="${news_detail.menu.menu_name}" escapeXml="false"></c:out>
        &nbsp;&nbsp; 发布时间:<c:out value="${news_detail.news_createTime}" escapeXml="false"></c:out>&nbsp;&nbsp; 浏览次数:<c:out value="${news_detail.news_viewTimes}" escapeXml="false"></c:out> </font>
        </p></td>
      </tr>
      <tr>
        <td height="12"><p align="center"></p></td>
      </tr>
      <tr >
        <td style="line-height: 150%" height="560" valign="top">
				<!--这里添加新闻的内容 -->
				<c:out value="${news_detail.news_content}" escapeXml="false"></c:out>
        </td>
      </tr>
      <tr>
        <td height="35">
          <p align="right"><input type="button" value=".打 印 本 页." onclick="javascript:window.print();">&nbsp;&nbsp;<input type="button" value=".关 闭 本 页." onclick="javascript:window.close();"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p></td>
      </tr>
      <tr>
      <td height="16"><hr></td>
      </tr>
      <tr>
        <td height="40">
          <p align="center">Copyright <font face="宋体">
          <span lang="en">© 2014
          </span>淳安县鸠坑万岁岭茶叶专业合作社</font></p>
        </td>
      </tr>
    </tbody></table>
    </div>
    
</body>
</html>
