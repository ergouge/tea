<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>行业动态</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="asset/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="asset/css/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/pagination_simple.css" type="text/css"/>
<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />

<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js"></script>
<script type="text/javascript" src="asset/js/pagination_simple.js"></script>
<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

</script>

<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
  </head>
  
  <body>
    <%@include file="../../header.jsp"%>
    <div id="templatemo_main_wrapper" style="height:700px;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1; ">
<!--     <div id="templatemo_main"> -->
	<div id="sidebar" class="left">
    	<div class="sidebar_box"><span class="bottom"></span>
            <h3>新闻</h3>   
            <div class="content"> 
                <ul class="sidebar_list">
                    <li><a href="page/news/EnterpriseNews.jsp">企业播报</a></li>
                    <li><a href="page/news/IndustryNews.jsp">行业动态</a></li>
                
                </ul>
            </div>
        </div>
       <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #000"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
    </div><!-- left end -->

        <div id="content" class="right">
 		<h2  style="float:left">行业动态</h2>
            <!-- 分页开始-->
             <div class="fenye" >
             <ul>
                 <li id="first" >首页</li>
                 <li id="top" onclick="topclick()">上一页</li>

                 <li class="xifenye"  id="xifenye">
                     <a id="xiye" style="">1</a>/<a id="mo">0</a>
                     <div class="xab" id="xab" style="display:none">
                         <ul id="uljia">

                         </ul>
                     </div>
                 </li>
                 <li id="down"  onclick="downclick()">下一页</a></li>
                 <li id="last" >末页</li>
             </ul>
            </div>
            <!-- 分页开始-->
            <div  class="cleaner h10"> </div>
             <ul id="new_ui"></ul> 
     </div><!-- right end -->
      <div class="cleaner h40"></div>  
<!--     </div> -->
    </div>
    <%@include file="../../footer.jsp" %>
    <!-- 隐藏域 -->
    <input type="hidden" value="${applicationScope.globleSize}" id="perPage" />
    <input type="hidden" value="10" id="modId" />
    <!-- 分页js -->
   
    <script type="text/javascript">
        $(document).ready(function(){
            //每页条数
         var perPage = $("#perPage").val(); 
     
            var menu_id = $("#modId").val();
            //获取总记录数
            var total;
             $.ajax({
                type : "POST",
                dataType : "json",
                async : false,
                data : {
                    'page' : 1,
                    'rows' : perPage,
                    'menu.id' : menu_id
                },
                url : 'news/news!getNewsListByParmsAndPages.sdo',
                success : function(msg) {
                    //总数
                    total = msg.total;
                    if (msg.rows.length > 0) {
                        $("#new_ui").empty();
                        $.each(msg.rows,function(index,obj){
                            $("#new_ui").append("<li><div class=\"left\">【"+obj.menu.menu_name+"】</div><div class=\"left1\"><a href='news/news!viewNews.sdo?news.id="+obj.id+"' target='_blank'>"+obj.news_title+"</a></div><div class=\"right\">"+obj.news_createTime+"</div></li>");
                        });

                    }
                }
            }); 
            var pageNums = Math.ceil(total/perPage);
            //初始化分页
            $('#mo').html(pageNums);
        });
    </script>
  </body>
</html>
