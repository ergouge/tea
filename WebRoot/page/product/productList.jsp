<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>鸠坑毛尖</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link href="asset/css/templatemo_style.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" href="asset/css/orman.css" type="text/css"
	media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css"
	media="screen" />
<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />
    <link rel="stylesheet" href="asset/css/pagination_simple.css" type="text/css"/>
    <style>
    	.pro_title_css{
    		width: 165px;
			overflow: hidden;
			white-space: nowrap;
			text-overflow: ellipsis;
    	}
    </style>
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>
<script type="text/javascript">
		
		ddsmoothmenu.init({
			mainmenuid: "templatemo_menu", //menu DIV id
			orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
			classname: 'ddsmoothmenu', //class added to menu's outer DIV
			//customtheme: ["#1c5a80", "#18374a"],
			contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
		})
		
		function clearText(field)
		{
		    if (field.defaultValue == field.value) field.value = '';
		    else if (field.value == '') field.value = field.defaultValue;
		}
		
		function productList(start,perPage){
		        var global_total = 0;
		            $.ajax({
		                type : "POST",
		                dataType : "json",
		                async : false,
		                data : {
		                    'start' : start,
		                    'limit' : perPage,
		                    'productType.id': $("#typeId").val()
		                },
		                url : 'product/product!getProductList.sdo',
		                success : function(msg) {
		                    //总数
		                    global_total = msg.total;
		                    if (msg.rows.length > 0) {
		                    	
		                        $("#content_main").empty();
		                        $("#content_main").append("<h2 class='producth2'>欢迎光临</h2>     ");
		
		
		                        $.each(msg.rows,function(index,obj){
		                            if((index+1)%4==0){
		                                $("#content_main").append("<div class='product_box no_margin_right'><a href=product/product!findProductFontById.sdo?product.id="+obj.id+"><img src='"+obj.pro_iconURL+"' alt='floral set 1' /></a>"+
		                                 "<h3 class='pro_title_css'>"+obj.pro_title+"</h3><p class='product_price' style='margin-bottom: 9px;'> 价格:<span class='price normal_price' style='font-size:12px;'>￥"+obj.pro_oriPrice+"</span>"+
		                                 "<span class='price special_price' style='font-size:12px;'>￥"+obj.pro_newPrice+"</span></p>  <p class='add_to_cart'> <a href='product/product!findProductFontById.sdo?product.id="+obj.id+"'>详细</a>"+
		                                 "<a href='"+obj.pro_TaoBaoURL+"'>商城</a> </p></div> ");
		                            }else{
		                                 $("#content_main").append("<div class='product_box'><a href=product/product!findProductFontById.sdo?product.id="+obj.id+"><img src='"+obj.pro_iconURL+"' alt='floral set 1' /></a>"+
		                                 "<h3 class='pro_title_css'>"+obj.pro_title+"</h3><p class='product_price' style='margin-bottom: 9px;'> 价格:<span class='price normal_price' style='font-size:12px;'>￥"+obj.pro_oriPrice+"</span>"+
		                                 "<span class='price special_price' style='font-size:12px;'>￥"+obj.pro_newPrice+"</span></p>  <p class='add_to_cart'> <a href='product/product!findProductFontById.sdo?product.id="+obj.id+"'>详细</a>"+
		                                 "<a href='"+obj.pro_TaoBaoURL+"'>商城</a> </p></div> ");
		                            }
		                          
		                        });     
		                        
		                    }
		                }
		
		            });
		        return global_total;
		};
		
	</script>


<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css"
	media="screen" />
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script>
</head>

<body>
	<%@include file="../../header.jsp"%>
	<div id="templatemo_main_wrapper"
		style="height:auto;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1; background-color: #E7EFD6">
<!-- 		<div id="templatemo_main"> -->
			<div id="sidebar" class="left">
				<div class="sidebar_box">
					<span class="bottom"></span>
					<h3>产品</h3>
					<div class="content">
						<ul class="sidebar_list">
							 <li><a href="page/product/productList.jsp?type=1" target="_self">鸠坑毛尖</a></li>
					<li><a href="page/product/productList.jsp?type=2" target="_self">红茶</a></li>
					<li><a href="page/product/productList.jsp?type=3" target="_self">千岛玉叶</a></li>
					<li><a href="page/product/productList.jsp?type=4" target="_self">千岛银针</a></li>
					<li><a href="page/product/productList.jsp?type=5" target="_self">春露</a></li>
					<li><a href="page/product/productList.jsp?type=6" target="_self">香茶</a></li>
					<li><a href="page/product/productList.jsp?type=7" target="_self">高绿</a></li>
					<li><a href="page/product/productList.jsp?type=8" target="_self">白茶</a></li>
					<li><a href="page/product/productList.jsp?type=9" target="_self">国红*金豪</a></li>
					<li><a href="page/product/productList.jsp?type=10" target="_self">国红*香螺</a></li>
					<li><a href="page/product/productList.jsp?type=11" target="_self">杭白菊</a></li>   

						</ul>
					</div>
				</div>
				  <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #000"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                 &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
			</div>

			<div id="content" class="right">
                <!-- 分页开始-->
                <div class="fenye" >
                    <ul>
                        <li id="first" >首页</li>
                        <li id="top" onclick="topclick()">上一页</li>

                        <li class="xifenye"  id="xifenye">
                            <a id="xiye" style="">1</a>/<a id="mo">0</a>
                            <div class="xab" id="xab" style="display:none">
                                <ul id="uljia">

                                </ul>
                            </div>
                        </li>
                        <li id="down"  onclick="downclick()">下一页</a></li>
                        <li id="last" >末页</li>
                    </ul>
                </div>
                <!-- 分页结束-->
                <div id="content_main">
						
                </div>
			</div>
			<div class="cleaner"></div>
<!-- 		</div> -->
		<!-- END of main -->
	</div>
	<%@include file="../../footer.jsp"%>
	 <!-- 隐藏域 -->
	 <%
	 	String sType = request.getParameter("type");
	 %>
    <div style="display:none">
	    <input type="hidden" value="12" id="perPage" />
    	<input type="hidden" value="<%=sType %>" id="typeId" />
    </div>
	
   
    <!-- 分页js -->
    <script type="text/javascript" src="asset/js/pagination_simple_product.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
           var total = productList(0,12);
            //每页条数
            var perPage = $("#perPage").val();

            var pageNums = Math.ceil(total/perPage);
            //初始化分页
            $('#mo').html(pageNums);
        });
    </script>
</body>
</html>
