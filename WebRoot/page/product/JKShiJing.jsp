<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>鸠坑诗经</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="asset/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="asset/css/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

</script>

<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
  </head>
  
  <body>
    <%@include file="../../header.jsp"%>
    <div id="templatemo_main_wrapper" style="height:1000px;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1; ">
    <div id="templatemo_main">
	<div id="sidebar" class="left">
    	<div class="sidebar_box"><span class="bottom"></span>
            <h3>产品</h3>   
            <div class="content"> 
                <ul class="sidebar_list">
                    <li><a href="page/product/JKMaoJian.jsp">鸠坑毛尖</a></li>
                    <li><a href="page/product/JKShiJing.jsp">鸠坑诗经</a></li>
                    
                </ul>
            </div>
        </div>
   <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #ffffff"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                 &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
    </div>
    
    <div id="content" class="right">
		<h2 clsss="producth2">欢迎光临</h2>		
        <div class="product_box">
            <a href="productdetail.html"><img src="asset/images/product/4.gif" alt="floral set 1" /></a>
      		<h3>鸠坑诗经一号</h3>
            <p class="product_price">$240</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                <a href="#">商城</a>
            </p>
        </div>        	
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/4.gif" alt="flowers 2" /></a>
            <h3>鸠坑诗经一号</h3>
          	<p class="product_price">$160</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>        	
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/4.gif" alt="floral 3" /></a>
            <h3>鸠坑诗经一号</h3>
          <p class="product_price">$140</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>      	
        <div class="product_box no_margin_right">
            <a href="productdetail.jsp"><img src="asset/images/product/1.gif" alt="flowers 4" /></a>
            <h3>鸠坑诗经一号</h3>
            <p class="product_price">$320</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>
        
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/2.gif" alt="floral set 5" /></a>
            <h3>鸠坑诗经二号</h3>
          	<p class="product_price">$150</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>        	
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/2.gif" alt="flowers 7" /></a>
            <h3>鸠坑诗经二号</h3>
            <p class="product_price">$110</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>        	
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/2.gif" alt="flower set 6" /></a>
            <h3>鸠坑诗经二号</h3>
            <p class="product_price">$130</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>        	
        <div class="product_box no_margin_right">
            <a href="productdetail.jsp"><img src="asset/images/product/2.gif" alt="floral 8" /></a>
            <h3>鸠坑诗经二号</h3>
            <p class="product_price">$170</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>
        
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/3.gif" alt="floral 8" /></a>
            <h3>鸠坑诗经三号</h3>
            <p class="product_price">$170</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div> 
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/3.gif" alt="floral 8" /></a>
            <h3>鸠坑诗经三号</h3>
            <p class="product_price">$170</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>    
        <div class="product_box">
            <a href="productdetail.jsp"><img src="asset/images/product/3.gif" alt="floral 8" /></a>
            <h3>鸠坑诗经三号</h3>
            <p class="product_price">$170</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>    
        <div class="product_box no_margin_right">
            <a href="productdetail.jsp"><img src="asset/images/product/3.gif" alt="floral 8" /></a>
            <h3>鸠坑诗经三号</h3>
            <p class="product_price">$170</p>
            <p class="add_to_cart">
                <a href="page/product/productDetail.jsp">详细</a>
                 <a href="#">商城</a>
            </p>
        </div>       
    </div>
    <div class="cleaner"></div>
</div> <!-- END of main -->
    </div>
    <%@include file="../../footer.jsp" %>
  </body>
</html>
