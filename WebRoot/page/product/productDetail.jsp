<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>鸠坑商品详情</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link href="asset/css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="asset/css/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />

<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

</script>
<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>

  </head>
  
  <body>
    <%@include file="../../header.jsp"%>
    <div id="templatemo_main_wrapper" style="height:auto;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1;background-color: #E7EFD6 ">
<!--     <div id="templatemo_main"> -->
	<div id="sidebar" class="left">
    	<div class="sidebar_box"><span class="bottom"></span>
            <h3>产品</h3>   
            <div class="content"> 
                <ul class="sidebar_list">
                    <li><a href="page/product/productList.jsp?type=1" target="_self">鸠坑毛尖</a></li>
					<li><a href="page/product/productList.jsp?type=2" target="_self">红茶</a></li>
					<li><a href="page/product/productList.jsp?type=3" target="_self">千岛玉叶</a></li>
					<li><a href="page/product/productList.jsp?type=4" target="_self">千岛银针</a></li>
					<li><a href="page/product/productList.jsp?type=5" target="_self">春露</a></li>
					<li><a href="page/product/productList.jsp?type=6" target="_self">香茶</a></li>
					<li><a href="page/product/productList.jsp?type=7" target="_self">高绿</a></li>
					<li><a href="page/product/productList.jsp?type=8" target="_self">白茶</a></li>
					<li><a href="page/product/productList.jsp?type=9" target="_self">国红*金豪</a></li>
					<li><a href="page/product/productList.jsp?type=10" target="_self">国红*香螺</a></li>
					<li><a href="page/product/productList.jsp?type=11" target="_self">杭白菊</a></li>                
                </ul>
            </div>
        </div>
          <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #000"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                 &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
    </div>
    
    <div id="content" class="right">
      	<h3 style="font-weight: bold;">${product.pro_title }</h3>
        <div class="content_half left">
        	<a rel="lightbox" href="${product.pro_iconURL }"><img style="width: 170px;height: 201px;" src="${product.pro_iconURL }" alt="yellow flowers" /></a>
        </div>
            <div class="content_half right">
                <table>
                    <tr>
                        <td width="130">原价:</td>
                        <td width="84"><span class="price normal_price">${product.pro_oriPrice }</span></td>
                    </tr>
                    <tr>
                        <td width="130">现价:</td>
                        <td width="84"> <span class="price special_price">${product.pro_newPrice }</span></td>
                    </tr>
                    <tr>
                        <td>商品状态:</td>
                        <td><strong>${product.pro_status }</strong></td>
                    </tr>

                </table>
                <div class="cleaner h20"></div>
                <span><a href="${product.pro_TaoBaoURL }" class="button">淘宝商城</a></span>
                <br>
               <span> <a href="${product.pro_weiURL }" class="button">微商城</a></span>
			</div>
            <div class="cleaner" style="margin-bottom: 20px;"></div>
            <hr style="margin-bottom: 10px;"/>
            <h4>产品描述</h4>
            <c:out value="${product.pro_intro }" escapeXml="false"></c:out>
			
            <div class="cleaner h40"></div>   
    </div>
    <div class="cleaner"></div>
<!-- </div>  -->
<!-- END of main -->
    </div>
    <%@include file="../../footer.jsp" %>
  </body>
</html>
