<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
		 <%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html lang="en" class="error_page">
    <head>
    <base href="<%=basePath%>">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Error Page - 404</title>
		<!-- Bootstrap framework -->
            <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css"  />
            <link rel="stylesheet" href="asset/bootstrap/css/bootstrap-responsive.min.css"  />
		<!-- main styles -->
            <link rel="stylesheet" href="asset/bootstrap/css/style.css"  />
			
            <link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
	</head>
	<body>

		<div class="error_box">
			<h1>404 Page/File not found</h1>
			<p>The page/file you've requested has been moved or taken off the site.</p>
			<a href="javascript:history.back()" class="back_link btn btn-small">回到上一页</a>
		</div>

		
	</body>
</html>
