<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" class="error_page">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Error Page - 404</title>
		<!-- Bootstrap framework -->
            <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css"  />
            <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap-responsive.min.css"  />
		<!-- main styles -->
            <link rel="stylesheet" href="../../asset/bootstrap/css/style.css"  />
			
            
	</head>
	<body>

		<div class="error_box">
			<h1>出错啦</h1>
			<p></p>
			<a href="javascript:history.back()" class="back_link btn btn-small">回到上一页</a>
		</div>

		
	</body>
</html>
