<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>企业文化</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <link href="asset/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="asset/css/orman.css" type="text/css" media="screen" />
<link rel="stylesheet" href="asset/css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="asset/css/ddsmoothmenu.css" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

</script>

<link rel="stylesheet" href="asset/css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="asset/js/slimbox2.js"></script> 
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
<script type="text/javascript" src="js/jquery.min.js"></script>
  </head>
  
  <body>
    <%@include file="../../header.jsp"%>
    <div id="templatemo_main_wrapper" style="height:700px;width:1000px;margin: 20px auto 30px;border: 1px solid #E1E1E1; ">
<!--     <div id="templatemo_main"> -->
	<div id="sidebar" class="left">
    	<div class="sidebar_box"><span class="bottom"></span>
            <h3>走进</h3>   
            <div class="content"> 
                <ul class="sidebar_list">
                    <li><a href="page/zouJingJiuKen/Introduce.jsp">企业简介</a></li>
                    <li><a href="page/zouJingJiuKen/History.jsp">鸠坑茶历史</a></li>
                    <li><a href="page/zouJingJiuKen/Culture.jsp">企业文化</a></li>
                    <li><a href="page/zouJingJiuKen/LeadCaring.jsp">领导关怀</a></li>
                    <li><a href="page/zouJingJiuKen/Honour.jsp">企业荣誉</a></li>
                    
                </ul>
            </div>
        </div>
        <!--  <div class="sidebar_box"><span class="bottom"></span>
            <h3>Weekly Special</h3>   
            <div class="content special"> 
                <img src="asset/images/1.gif" alt="Flowers" />
                <a href="#">鸠坑毛尖一号</a>
                <p>
                	Price:
                    <span class="price normal_price">$160</span>&nbsp;&nbsp;
                    <span class="price special_price">$100</span>
                </p>
            </div>
        </div>-->
        <div class="sidebar_box"><span class="bottom"></span>
            <h3>通信地址</h3>   
            <div class="content special" style="color: #000"> 
                <strong style="margin-left: -95px">地址：</strong>杭州市江干区<br/>
                   &nbsp;&nbsp;凯旋路70号159#<br/>
				<strong style="margin-left: -50px">电话：</strong>0571-86957651<br />
				<strong style="margin-left: -63px">手机：</strong>18958191388<br />
	            <strong style="margin-left: -59px">传真：</strong>028-86957651<br />
            </div>
        </div>
    </div><!-- left end -->
     <div id="content" class="right">
     <h2>企业文化</h2>
     <span id="lblArticleContent" class="content">
      
	 </span>
     </div><!-- right end -->
      <div class="cleaner h40"></div>  
<!--     </div> -->
    </div>
    <%@include file="../../footer.jsp" %>
    <script type="text/javascript">
$(document).ready(function(){

//获取企业文化
$.ajax({
	type : 'post',// 使用post方法访问后台
	async: false,
	data: {'menu.id':5},
	dataType : 'json',// 返回json格式的数据
	url : 'introduce/introduce!getOneObj.sdo',// 要访问的后台地址
	success : function(data) {

		if(data!=''){			
			$("#lblArticleContent").html(data.intro_content);
			
		}else{
			$("#lblArticleContent").html("暂无内容。");
		}
	
	},
	error : function(XMLHttpRequest, textStatus, errorThrown) {
		
	}
});})

</script>
  </body>
</html>
