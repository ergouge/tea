<!-- 
ergouge
2015年1月9日 下午10:46:35
UTF-8
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE></TITLE>
	<base href="<%=basePath%>">
	<META http-equiv=Content-Type content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="asset/smoke/smoke.css">
</HEAD>
<body>

	<script type="text/javascript" src="asset/smoke/smoke.min.js"></script>
	<script type="text/javascript">
		smoke.alert("您还未登录，或者登录状态已失效", function(e){
			top.location.href="login.jsp";
		}, {
			ok: "确定",
			cancel: "Nope",
			classname: "custom-class"
		});
	</script>
	<%--您还未登录，或者登录状态已失效，请<a href="login.jsp">重新登录</a>--%>
	<%--<script type="text/javascript">
	 var bln = window.alert("未登录，请重新登录");
	 parent.location.href="login.jsp";
	</script>--%>
</body>
</HTML>

