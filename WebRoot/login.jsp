<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>系统登录</title>
<link href="asset/css/login.css" rel="stylesheet" rev="stylesheet" type="text/css" media="all" />
<link href="asset/css/footer.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/jquery.SuperSlide.js"></script>
<script type="text/javascript" src="asset/js/Validform_v5.3.2_min.js"></script>

<script>
	var g_obj = {};
	$(document).ready(function() {
		
		
	/* 	var tip = "${MSG}";
		if(tip != "") {
			$(".error-box").text(tip);
			$(".error-box").show();
		}
		else{
			//$(".error-box").hide();
		} */
		
		
		$(".i-text").focus(function() {
			$(this).addClass('h-light');
		});

		$(".i-text").focusout(function() {
			$(this).removeClass('h-light');
		});
		/* $(".registerform").Validform({
			tiptype : function(msg, o, cssctl) {
				var objtip = $(".error-box");
				cssctl(objtip, o.type);
				objtip.text(msg);
			},
			ajaxPost : true
		}); */
		
		$("#send-btn").click(function(){
			var uname = $("#username").val();
			var upwd = $("#password").val();
			var code = $("#yzm").val();
			if(uname == "") {
				$(".error-box").text("请输入用户名！");
				return false;
			}
			if(upwd == "") {
				$(".error-box").text("请输入密码！");
				return false;
			}
			if(code == "") {
				$(".error-box").text("请输入验证码！");
				return false;
			}
			$.ajax({
				type : 'get',// 使用post方法访问后台
				async: false,
				data: {
					'indentifyCode' : code
					},
				dataType : 'json',// 返回json格式的数据
				url : 'idcode/identifyCode!validateIdCode.sdo',// 要访问的后台地址
				success : function(data) {
					if(data==true){
						console.info(2);
						var flag = submit_form();
						if(flag) {
							window.location.href = g_obj.url;
						}
					}
					else{
						$(".error-box").text("验证码出错，请重试！");
						changImgae($("#identifyImage")[0]);
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		});
		
		
		
		
	});
	/*重新生成验证码*/
	function changImgae(obj){
		obj.src = obj.src+"?";
	}
	function submit_form(){
		var result = false;
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {
				'user.login_name' : $("#username").val(),
				'user.user_pwd' : $("#password").val()
				},
			dataType : 'json',// 返回json格式的数据
			url : 'user/user!login.sdo',// 要访问的后台地址
			success : function(data) {
				if(data.result){
					result = true;
					g_obj.url = data.url;
				}
				else{
					$(".error-box").text(data.msg);
					changImgae($("#identifyImage")[0]);
					return false;
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
		return result;
	}
</script>

</head>

<body>


<div class="header" style="width:1000px;"><!-- background-image: url(asset/images/login_header_bg.png) -->
	<h1 class="headerLogo"><a title="后台管理系统" target="_blank" href="#"><img alt="logo" src="asset/images/logo.gif"></a></h1>
	<div class="headerNav">
		<a target="_blank" href="#">首页</a>
		<a target="_blank" href="#">关于</a>
		<a target="_blank" href="#">开发团队</a>
		<a target="_blank" href="#">意见反馈</a>
		<a target="_blank" href="#">帮助</a>	
	</div>
</div>

<div class="banner">

<div class="login-aside">
  <div id="o-box-up"></div>
  <div id="o-box-down"  style="table-layout:fixed;">
   <div class="error-box"></div>
   
   <form class="registerform">
   <div class="fm-item">
	   <label for="logonId" class="form-label">系统登录：</label>
	   <input name="user.login_name" type="text"  maxlength="100" id="username" class="i-text" datatype="s3-18" errormsg="用户名至少3个字符,最多18个字符！"  >
       <div class="ui-form-explain"></div>
  </div>
  
  <div class="fm-item">
	   <label for="logonId" class="form-label">登录密码：</label>
	   <input type="password" name="user.user_pwd"  maxlength="100" id="password" class="i-text" datatype="*6-16"  errormsg="密码范围在6~16位之间！">    
       <div class="ui-form-explain"></div>
  </div>
  
  <div class="fm-item pos-r">
	   <label for="logonId" class="form-label">验证码</label>
	   <input type="text"  maxlength="100" id="yzm" class="i-text yzm"  >    
       <div class="ui-form-explain">
<!--        <img src="asset/images/yzm.jpg"  /> -->
       <img src="idcode/identifyCode!generateIdCode.sdo" class="yzm-img" id="identifyImage" onclick="changImgae(this)"/>
       </div>
  </div>
  
  <div class="fm-item">
	   <label for="logonId" class="form-label"></label>
	   <button tabindex="4" id="send-btn" class="btn-login" type="button"></button> 
       <div class="ui-form-explain"></div>
  </div>
  
  </form>
  
  </div>

</div>

	<div class="bd">
		<ul>
			<li style="background:url(asset/images/home/login.jpg) #CCE1F3 center 0 no-repeat;background-size:1000px 478px;"></li>
			<!--<li style="background:url(themes/1.gif) #BCE0FF center 0 repeat-y;"><a target="_blank" href="#"></a></li>-->
		</ul>
	</div>

	<div class="hd"><ul></ul></div>
</div>
<!-- <script type="text/javascript">jQuery(".banner").slide({ titCell:".hd ul", mainCell:".bd ul", effect:"fold",  autoPlay:true, autoPage:true, trigger:"click" });</script> -->
<!-- <div class="banner-shadow"></div> -->
<%@include file="footer.jsp"%>

</body>