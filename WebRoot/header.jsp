<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link href="asset/css/Article.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/Article.js"></script>
    <div id="header_logo">
		<div class="logo autom"  align="center" >
		   <div class="left" style="float:left">
		   		<img src="asset/images/logo.gif" style="float:left"/> 
		   	</div>
           <div style="float:left ;margin-top:35px"><h2 style="font-size:200%;color:#000;margin-left:20px;">淳安县鸠坑万岁岭茶叶专业合作社</h2></div>
           
			<div style="float:right;margin-top:15px">
				<div style="float:right">
					<a href="login.jsp" style="color: gray;">会员登录</a>&nbsp;|&nbsp;
					<a style="color: gray;" href="">设为主页</a>&nbsp;|&nbsp;
					<a style="color: gray;" href="">加入收藏</a>
				</div>
				<div style="margin-top:50px;margin-left:50px">
					<img src="asset/images/phone.gif" style="vertical-align: top;"/>
					<h2 style="display:inline;font-size:120%;color:green;font-weight: bold;letter-spacing: 0.2px;">客服咨询电话：0571-86957651</h2>
				</div>
			</div>
		</div>
		
	</div>
<div id="Top">
	<div class="Toolbar1">
		<div class="CentreBox">
			<div class="Menu">
				<ul class="List1">
					<li ><a href="#" target="_self">首页</a></li>
					<li ><a href="page/zouJingJiuKen/Introduce.jsp" target="_self">走进</a></li>
					<li ><a href="page/news/EnterpriseNews.jsp" target="_self">新闻</a></li>
					<li><a href="page/product/productList.jsp?type=0" target="_self">产品</a></li>
					<li><a href="page/member/Activity.jsp" target="_self">会员专区</a></li>
                    <li><a href="http://shop104452439.taobao.com/?spm=a230r.7195193.1997079397.2.py3QU4" target="_blank">营销网络</a></li>
                    <li><a href="page/contact/ContactUs.jsp" target="_self">联系我们</a></li>
                    <li><a href="http://shop104452439.taobao.com/?spm=a230r.7195193.1997079397.2.py3QU4" target="_blank">商城区</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="Toolbar2">
		<div class="CentreBox">
			<div class="Menu Hide">
				<ul>										
				</ul>
			</div>
			<div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/zouJingJiuKen/Introduce.jsp" target="_self">企业简介</a></li>
					<li><a href="page/zouJingJiuKen/History.jsp" target="_self">鸠坑茶历史</a></li>
					<li><a href="page/zouJingJiuKen/Culture.jsp" target="_self">企业文化</a></li>
					<li><a href="page/zouJingJiuKen/LeadCaring.jsp" target="_self">领导关怀</a></li>
					<li><a href="page/zouJingJiuKen/Honour.jsp" target="_self">企业荣誉</a></li>
					
				</ul>
			</div>
			<div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/news/EnterpriseNews.jsp" target="_self">企业播报</a></li>
					<li><a href="page/news/IndustryNews.jsp" target="_self">行业动态</a></li>
					
					
				</ul>
			</div>
			<div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/product/productList.jsp?type=1" target="_self">鸠坑毛尖</a></li>
					<li><a href="page/product/productList.jsp?type=2" target="_self">红茶</a></li>
					<li><a href="page/product/productList.jsp?type=3" target="_self">千岛玉叶</a></li>
					<li><a href="page/product/productList.jsp?type=4" target="_self">千岛银针</a></li>
					<li><a href="page/product/productList.jsp?type=5" target="_self">春露</a></li>
					<li><a href="page/product/productList.jsp?type=6" target="_self">香茶</a></li>
					<li><a href="page/product/productList.jsp?type=7" target="_self">高绿</a></li>
					<li><a href="page/product/productList.jsp?type=8" target="_self">白茶</a></li>
					<li><a href="page/product/productList.jsp?type=9" target="_self">国红*金豪</a></li>
					<li><a href="page/product/productList.jsp?type=10" target="_self">国红*香螺</a></li>
					<li><a href="page/product/productList.jsp?type=11" target="_self">杭白菊</a></li>
				
				</ul>
			</div>
			<div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/member/VipDiscount.jsp" target="_self">VIP优惠</a></li>
					<li><a href="page/member/Activity.jsp" target="_self">会员活动</a></li>
					<li><a href="page/member/Announcement.jsp" target="_self">会员公告</a></li>
					<li><a href="page/member/Data.jsp" target="_self">会员资料</a></li>
					<li><a href="page/member/Apply.jsp" target="_self">会员申请</a></li>
				</ul>
			</div>
            <div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/mall/physicalStore.jsp" target="_self">直营店</a></li>
					<li><a href="#" target="_self">微信商城</a></li>
					<li><a href="http://shop104452439.taobao.com/?spm=a230r.7195193.1997079397.2.py3QU4" target="_blank">淘宝店</a></li>
				</ul>
			</div>
            <div class="Menu Hide">
				<ul>
					<li class="Select"><a href="page/contact/Recruitment.jsp" target="_self">人才招聘</a></li>
					<li><a href="page/contact/JoinIn.jsp" target="_self">加盟在线申请</a></li>
					<li><a href="page/contact/ContactUs.jsp" target="_self">联系方式</a></li>
				</ul>
			</div>
            
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $.ajax({
	    type: "POST",
	    url: "shop/shop!findUrl.sdo",
	    async:false,
	    data:{
	    },
	    dataType: "json",
	    success: function(msg){
	    	for(var i=0;i<msg.length;i++){
	    		$(".mall:eq("+i+") a").attr("href",msg[i].shop_url);
	    }
	    },
	           error:function(XMLHttpRequest, textStatus, errorThrown){
	    	
	    }
		});
	});
</script>