<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<base href="<%=basePath%>">

<link rel="stylesheet" href="admin/lib/smoke/smoke.css"/>

<script type="text/javascript" charset="utf-8" src="admin/lib/smoke/smoke.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		getUrl();
	});
	function getUrl(){
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {shopId:1},
			dataType : 'json',// 返回json格式的数据
			url : 'shop/shop!findShopById.action',// 要访问的后台地址
			success : function(data) {
				$('#txtarea_direct').val(data.shop_url);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}	
		});
	}

	function update(){
		var shopUrl = $("#txtarea_direct").val();
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {'shop.id':1,'shop.shop_url':shopUrl},
			dataType : 'json',// 返回json格式的数据
			url : 'shop/shop!updateShop.action',// 要访问的后台地址
			success : function(data) {
				if(data.result) {
					smoke.alert('修改成功');
					$(".main_content").load("admin/page/mall/directSaleStore.jsp");
				} 
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}	
		});
	}
</script>

<div>
	
	<div class="row-fluid">
		<label>直营店网址</label>
		<hr>
		<textarea name="txtarea_direct" id="txtarea_direct" cols="10" rows="3" class="span8"></textarea>
	</div>
	<div class="row-fluid">
		<button class="btn btn-info" id="enter" onclick="update()">确定</button>
		<button class="btn" id="cancel">取消</button>
	</div>

</div>


