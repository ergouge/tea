<script>
window.UEDITOR_HOME_URL = "asset/ueditor/";
</script>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<base href="<%=basePath%>">
<link rel="stylesheet" href="admin/lib/smoke/smoke.css"/>

<script type="text/javascript" charset="utf-8" src="admin/lib/smoke/smoke.min.js"></script>

<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.all.min.js"> </script>

<script type="text/javascript" charset="utf-8" src="asset/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
	var intro_id = 0;
	
	$(document).ready(function(){
		var all_content = "暂无内容";
		//获取到待修改对象
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			
			data: {
				'menu.id' : $('#menu_id').val()
				},
			dataType : 'json',// 返回json格式的数据
			url : 'introduce/introduce!getOneObj.action',// 要访问的后台地址
			success : function(data) {
				all_content = data.intro_content;
				intro_id = data.id;
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
		//ueditor实例
		var ue = UE.getEditor('editor');
		ue.addListener("ready", function () {
		    // editor准备好之后才可以使用
		    ue.setContent(all_content);
		});
	});
	
	//修改
	$('#enter').click(function (){
		var htmlContent = UE.getEditor('editor').getContent();	
		var urlStr = "introduce/introduce!updateObj.action";
		//发送异步的请求
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {
				'introduce.id' : intro_id,
				'introduce.intro_content': htmlContent,
				},
			dataType : 'json',// 返回json格式的数据
			url : urlStr,// 要访问的后台地址
			success : function(data) {
				if(data.result) {
					smoke.signal(data.msg, function(e){
					}, {
						duration: 3000,
						classname: "custom-class"
					});
				} else {
					smoke.signal(data.msg, function(e){
					}, {
						duration: 3000,
						classname: "custom-class"
					});
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	})

</script>

<div>
	<div class="row-fluid">
		<button class="btn btn-info" id="enter">确定</button>
		<button class="btn" id="cancel">取消</button>
	</div>
	<hr>
	<input type="hidden" value="3" id="menu_id" />
	<script id="editor" type="text/plain" style="width: 1024px; height: 500px;"></script>
</div>


