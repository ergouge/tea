<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="row-fluid">
		<div class="span5 ">
				<h3 class="heading">Visitors by Country <small>last week</small></h3>
			<div id="fl_2" style="height:200px;width:80%;margin:50px auto 0"></div>
		</div>
		<div class="span7">
				<div class="heading clearfix">
					<h3 class="pull-left">Another Chart</h3>
					<span class="pull-right label label-info ttip_t" title="Here is a sample info tooltip">Info</span>
				</div>
			<div id="fl_1" style="height:270px;width:100%;margin:15px auto 0"></div>
		</div>
	</div>