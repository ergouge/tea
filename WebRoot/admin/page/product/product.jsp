<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript" src="admin/javascript/userDefine/newsManager.js"></script>
<link rel="stylesheet" href="admin/bootstrap/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="admin/bootstrap/css/pagination.css" type="text/css" />
<script type="text/javascript" src="admin/bootstrap/js/jquery.pagination.js"></script>

<script type="text/javascript">

	var perPage = $("#perPage").val();       
	var total = (function() {
	var result;
    $.ajax({
        type : "POST",
        dataType : "json",
        async : false,
        data : {start : 0,limit : 0,menu_id : $('#menu_id').val()
        },
        url : "product/product!getProductList.action",
        success : function(msg) {
                result = msg.total;
        }
    });
    return result;
})();
        var gPage_index = 0;
        var vPage_index = 0;
        function pageselectCallback(page_index, jq){
            var items_per_page = perPage;//每页显示的内容数量
            var max_elem = Math.min((page_index+1) * items_per_page, total);
            function numPages() {
                return Math.ceil(total/items_per_page);
            }
            $("#currentPage").val(page_index+1);
            gPage_index = page_index+1;
            $("#numPage").html("/"+numPages(total)+"页");
            $.ajax({
                type : "POST",
                dataType : "json",
                async : false,
                url : 'product/product!getProductList.action', //后台数据提取
                data : {start:page_index * items_per_page,limit:items_per_page,menu_id : $('#menu_id').val()},
                success : function(msg) {
                	 if (msg.rows.length > 0) {
                	   $("#productList").empty();
                       //var dataObj=eval("("+data+")");//转换为json对象
                       $.each(msg.rows,function(index,data){
                    	var content ="<tr><td  style='text-align: center;'><input type='checkbox' value='"+data.id+"' name='chkItem' class='product_id'/></td>"
       					+"<td style='text-align: center;'>"+data.id+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_title+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_remark+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_oriPrice+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_newPrice+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_status+"</td>"
       					+"<td style='text-align: center;'>"+data.pro_num+"</td>"
       					+"<td style='text-align: center;'>"+data.type.type_name+"</td>"
       					+"<td style='text-align: center;'><a data-toggle='modal' class='btn btn-success btn-small' href='javascript:productDetail("+data.id+")'><i class='splashy-view_list'></i>详情</a></td>"
       					+"<td style='text-align: center;'><a class='btn btn-info btn-small' href='javascript:updateProduct("+data.id+")'><i class='splashy-sprocket_light'></i>修改</a></td>"
       					+"<td style='text-align: center;'><a class='btn btn-danger btn-small' href='javascript:deleteProduct("+data.id+")'><i class='splashy-error_x'></i> 删除</a></td>"
       					+"</tr>";
       					$("#productList").append(content);
                       });
                        }
                    }
                });
            return false;
        }

        //设置基本参数
        function getOptionsFromForm(){
            var opt = {callback: pageselectCallback};
            opt.items_per_page = parseInt(perPage);             //每页中的内容行数
            opt.num_display_entries = parseInt("5"); //分页中央显示页码数量
            opt.num_edge_entries = parseInt("1");     //分页两侧显示页码数量
            opt.prev_text = "<<";
            opt.next_text = ">>";
 
            return opt;
        }

        //设置基本参数
        $(document).ready(function(){
        	$("#addProduct").click(function(){
        		$(".main_content").load("admin/page/product/addProduct.jsp");
        	});
            var optInit = getOptionsFromForm();
            $("#Pagination").pagination(total, optInit);
            $("#goPage").click(function(){
                var opt = getOptionsFromForm();
                var currentPage = $("#currentPage").val();
                var maxPage = Math.ceil(total/opt.items_per_page);
                if(currentPage>maxPage || currentPage<0){
                    alert("请输入范围内的页码");
                    $("#currentPage").val(gPage_index);
                }else{
                    opt.current_page = currentPage-1;
                    $("#Pagination").pagination(total, opt);
                }
            });
            //全选
            $('table th input:checkbox').on('click' , function(){
         		var that = this;
         		$(this).closest('table').find('tr > td:first-child input:checkbox')
         		.each(function(){
         			this.checked = that.checked;
         			$(this).closest('tr').toggleClass('selected');
         		});
         	});           
        });
function updateProduct(id){
	$(".main_content").load("admin/page/product/updateProduct.jsp?product_id="+id);
}
function  deleteProduct(product_id){
		/* alert(product_id); */
		smoke.confirm("是否需要删除该产品?", function(e){
	if (e){

	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		dataType : 'json',// 返回json格式的数据
		data: {
			'product.id' : product_id
			},
		url : 'product/product!deleteProductById.action',// 要访问的后台地址
		success : function(data) {
			/* console.dir(data); */
			smoke.alert(data.msg, function(e){});
			$(".main_content").load("admin/page/product/product.jsp");
		},	
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			top.location.href="temp.jsp";
		}
		});
	}else{

	}
}, {
	ok: "删除",
	cancel: "取消",
	classname: "custom-class",
	reverseButtons: true

});
};
function deleteProducts(){
	var product_id="";
	alert($(".product_id").length);
	for(var i=0;i<$(".product_id").length;i++){
		if($(".product_id:eq("+i+")").attr("checked")=="checked"){
			product_id+=$(".product_id:eq("+i+")").val()+",";
		}
	}
	if(product_id==""){
	smoke.alert("请选择要删除的产品", function(e){
	
}, {
	ok: "确定",
	classname: "custom-class"
});
	}else{

smoke.confirm("是否需要删除这些产品?", function(e){
	if (e){

	
		$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		dataType : 'json',// 返回json格式的数据
		data: {
			'ids' : product_id
			},
		url : 'product/product!deleteProduct.action',// 要访问的后台地址
		success : function(data) {
			/* console.dir(data); */
			smoke.alert(data.msg, function(e){});
			$("#selectAll").removeAttr("checked");
			$(".main_content").load("admin/page/product/product.jsp");
		},	
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			top.location.href="temp.jsp";
		}
		});
	}else{

	}
}, {
	ok: "删除",
	cancel: "取消",
	classname: "custom-class",
	reverseButtons: true

});

	}
};
function productDetail(product_id){
	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		data: {
			'product.id' : product_id
			},
		dataType : 'json',// 返回json格式的数据
		url : 'product/product!findProductById.action',// 要访问的后台地址
		success : function(data) {
			$(".modal-title").html(data.product_title);
			$(".modal-body").html(data.product_intro);
			$("#myModal").modal(); 
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			top.location.href="temp.jsp";
		}	
	});
}

	
$("#selectAll").click(function(){
	if($(this).attr("checked") == "checked"){
		$(".product_id").attr("checked","checked");
	}else{
		$(".product_id").removeAttr("checked");
	}
});

$("#search").click(function(){
	var title = $("#search_title").val();
	var content = $("#search_content").val();
	var author = $("#search_author").val();
	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		data: {
			'menu.id' : $('#menu_id').val(),
			'product.product_title':title,
			'product.product_intro':content,
			'product.user.user_name':author
			},
		dataType : 'json',// 返回json格式的数据
		url : 'product/product!getProductListByParmsAndPages.action',// 要访问的后台地址
		success : function(data) {
			$("#productList").empty();
			for(var i=0;i<data.total;i++){
				var checkBox='<td  class="center"><input type="checkbox"></td>';
				var product_id="<td class='center'>"+data.rows[i].id+"</td>";
				var product_title='<td class="center">'+data.rows[i].product_title+"</td>";
				var product_content='<td class="center">'+data.rows[i].product_content+"</td>";
			    var product_addUser='<td class="center">'+data.rows[i].user.user_name+"</td>";				
				var product_createTime='<td class="center">'+data.rows[i].product_createTime+"</td>";
				var product_modifyTime='<td class="center">'+data.rows[i].product_modifyTime+"</td>";
				var product_viewTimes='<td class="center">'+data.rows[i].product_viewTimes+"</td>";
				var action1='<td class="center"><a class="btn btn-success btn-small"  href="javascript:newDetail('+data.rows[i].id+')" ><i class="splashy-view_list"></i>详情</a></td>'
				var action2='<td class="center"><a class="btn btn-info btn-small" href="javascript:void(0)"><i class="splashy-sprocket_light"></i>修改</a></td>'
				var action3="<td class='center'><a class='btn btn-danger btn-small' href='javascript:deleteProduct("+data.id+")'><i class='splashy-error_x'></i> 删除</a></td>"
				$("#productList").append("<tr>", checkBox,product_id,product_title,product_content,product_addUser,product_createTime,product_modifyTime,product_viewTimes,action1,action2,action3,"</tr>");
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
});
</script>
<input type="hidden" value="${applicationScope.globleSize} " id="perPage" />
<div class="row-fluid">
 	<div class="span3" >
 		<div class="btn-group">
 			<a id="addProduct" class="btn btn-success" href="javascript:void(0)"><i class="splashy-add"></i> 添加产品</a> 
 			<a class="btn btn-danger" href="javascript:deleteProducts()"><i class="splashy-remove_minus_sign"></i> 删除产品</a> 
 		</div>
	</div>
 	<div class="span9 " align="right"> 
		<form action="" class="form-inline" >
			<div class="form-group">
				<input type="text" style="width: 120px;" placeholder="请输入产品标题">
				<input type="text"  style="width: 120px;" placeholder="请输入产品内容">
				<input type="text" style="width: 140px;"  placeholder="请输入添加人员姓名">
				<a class="btn btn-info" href="javascript:void(0)"><i class="splashy-zoom"></i>搜索</a>
			</div>
		</form>
	</div>  
</div>
<div class="row-fluid"  style="margin-top: 0px;">
	<div class="span12">
		<table class="table table-striped table-bordered   dataTable dataTables_wrapper" id="dt_a">
			<thead>
				<tr >
					<th class="center"><input type="checkbox"></th>
					<th class="center">ID</th>
					<th class="center">产品标题</th>
					<th class="center">产品添加时间</th>
					<th class="center">产品原价格</th>
					<th class="center">产品最新价格</th>
					<th class="center">产品状态</th>
					<th class="center">产品数量</th>
					<th class="center">产品类型</th>
					<th class="center" colspan="3" style="width: 260px;">动作</th>
				</tr>
			</thead> 
			<tbody id="productList">
			</tbody>
		</table>
	</div>
</div>
 <!--分页开始-->
    <div class="row-fluid" id="pageNum" style="margin-top: 10px">
        <div class="span12">
            <div class="pagination"
                style="float:right;margin:0px 20px 0px 0px;">
                <div style="float:left;">
                    <ul id="Pagination">
                    </ul>
               </div>
                <div
                    style="vertical-align:top; height:30px;width:auto;float:left;">
                    <input id="currentPage" type="text"
                        style="width: 38px;height:28px;margin-left: 5px; text-align:center" />
                    <label id="numPage" style="color:darkgrey;font-size: 15px;display: inline-block;"></label>
                    <button id="goPage"
                        style="width: 55px;height:29px; vertical-align: top;"
                        class="btn btn-small btn-primary">跳转</button>
                </div>
            </div>
        </div>
    </div>
<!--分页结束-->

<div class="modal fade "  id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >&times;</button>
					<h4  class="modal-title">产品标题</h4>
				</div>
				<div class="modal-body">
					<p>这里是弹框的具体内容</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" >保存</button>
				</div>
			</div>
		</div>
</div>
<script type="text/javascript">
	
</script>