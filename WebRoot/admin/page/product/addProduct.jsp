<script>
window.UEDITOR_HOME_URL = "asset/ueditor/";
</script>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8" src="admin/lib/form/jquery.form.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.all.min.js"> </script>

<script type="text/javascript" charset="utf-8" src="asset/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
	var all_content="";
	var htmlContent;
	$(document).ready(function(){					
		//ueditor实例
		var ue = UE.getEditor('editor');
	/* 	ue.addListener("ready", function () {
		    // editor准备好之后才可以使用
		    ue.setContent(all_content);
		}); */

		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {'menu.id':3},
			dataType : 'json',// 返回json格式的数据
			url : 'product/product!getProductTypes.sdo',// 要访问的后台地址
			success : function(data) {
				console.dir(data);
				if(data){			
					for(var i = 0; i < data.length; i ++) {
						$("#productType").append("<option value='"+data[i].id+"'>"+data[i].type_name+"</option>");
					}
				}else{
					$("#productType").append("<option>加载失败，请稍后重试</option>");
					$("#enter").attr("disabled", true);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				
			}
		});
		/* $("#level_1").change(function(){
			$("#level_2 option").remove();
		 	if($("#level_1").val() == "绿茶") {
				$("#level_2").append("<option value='1'>鸠坑毛尖</option>");
				$("#level_2").append("<option value='2'>千岛玉叶</option>");
				$("#level_2").append("<option value='3'>千岛银针</option>");
				$("#level_2").append("<option value='4'>西湖龙井</option>");
			}
			if($("#level_1").val() == "红茶") {
				$("#level_2").append("<option value='5'>鸠坑红茶</option>");
				$("#level_2").append("<option value='6'>九曲红梅</option>");
				$("#level_2").append("<option value='7'>普洱古树红茶</option>");
			}
			if($("#level_1").val() == "普洱") {
				$("#level_2").append("<option value='8'>普洱云茶</option>");
				$("#level_2").append("<option value='9'>古树生普</option>");
				$("#level_2").append("<option value='10'>普洱熟茶</option>");
			}  
		}); */

		$('#pro_oriPrice').focusout(function() {
			$('#pro_newPrice').val($('#pro_oriPrice').val());
		});
	});
	
	//修改
	$("#enter").click(function (){
		if(!checkForm()){
			return false;
		}
		htmlContent = UE.getEditor('editor').getContent();
		$("#pro_info").val(htmlContent);
		var urlStr = 'product/product!addProduct.action';
		//发送异步的请求
		$("#form1").ajaxSubmit({
			type : 'post',// 使用post方法访问后台
			async: false,
			dataType : 'json',// 返回json格式的数据
			url : urlStr,// 要访问的后台地址
			success : function(data) {
				console.dir(data);
				if(data.result) {
					smoke.alert('添加成功.');
					$(".main_content").load("admin/page/product/product.jsp");
				} else {
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				top.location.href="page/error/error.jsp";
			}
		});
	})
     function checkForm(){

	    if($('#pro_title').val()==null||$('#pro_title').val()==""){
		   smoke.alert('产品名称不能为空.');
		   return false;
		}
		if($('#productImg').val() == null || $('#productImg').val() == "") {
			smoke.alert('产品图片不能为空');
			return false;
		}
		 if($('#pro_oriPrice').val() == null || $('#pro_oriPrice').val() == "") {
			 smoke.alert('原价不能为空');
			 return false;
		 }
		 if($('#pro_newPrice').val() == null || $('#pro_newPrice').val() == "") {
			 smoke.alert('现价不能为空');
			 return false;
		 }
		return true;
	}
</script>

<div class="row-fluid">
	<div class="span12">
		<form id="form1" enctype="multipart/form-data">
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">产品名称</span>
						</p>
						<input type="text" name="product.pro_title" id="pro_title"/>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">选择产品图片（最佳分辨率为200*150）</span>
						</p>
						<div data-provides="fileupload" class="fileupload fileupload-new">
							<input type="hidden">
							<div style="width: 200px; height: 150px;"
								class="fileupload-new thumbnail">
								<img src="admin/img/200x150/EFEFEF/AAAAAA&text=no+image.gif"
									alt="" />
							</div>
							<div
								style="max-width: 200px; max-height: 150px; line-height: 0px;"
								class="fileupload-preview fileupload-exists thumbnail"></div>
							<div>
								<span class="btn btn-file"><span class="fileupload-new">选择图片</span><span
									class="fileupload-exists">更换</span> <input type="file"
									name="productImg" id="productImg"/> </span> <a data-dismiss="fileupload"
									class="btn fileupload-exists" href="#">移除</a>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">一级类别</span>
						</p>
						<select name="productType.id" id="productType">
							
						</select>
					</div>
					<!-- <div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">二级类别</span>
						</p>
						<select name="productType.id" id="level_2">
							<option value="1">鸠坑毛尖</option>
							<option value="2">千岛玉叶</option>
							<option value="3">千岛银针</option>
							<option value="4">西湖龙井</option>
						</select>
					</div> -->
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">产品原价</span>
						</p>
							<!-- <label for="sp_dec">Decimal</label> -->
<!-- 							<input type="text" id="sp_dec" name="sp_dec" class="span12" value="10.25"/> -->
						<input type="text" name="product.pro_oriPrice" id="pro_oriPrice"/>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">产品现价</span>
						</p>
						<input type="text" name="product.pro_newPrice" id="pro_newPrice" />
					</div>
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">

						<p class="sepH_c">
							<span class="label label-inverse">产品状态</span>
						</p>
						<select name="product.pro_status" id="pro_status">
							<option value="新品">新品</option>
							<option value="热销">热销</option>
							<option value="下架">下架</option>
						</select>
					</div>
					<div class="span8">

						<p class="sepH_c">
							<span class="label label-inverse">产品数量</span>
						</p>
						<input type="text" name="product.pro_num" />
					</div>
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">淘宝店对应商品网址</span>
						</p>

						<textarea name="product.pro_TaoBaoURL" id="txtarea_reg" cols="30"  
							rows="3"></textarea>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">微店对应商品网址</span>
						</p>
						<textarea name="product.pro_weiURL" id="txtarea_reg" cols="30" 
							rows="3"></textarea>
					</div>
				</div>
			</div>
			<input type="hidden" value="12" id="menu_id" name="menu.id"/>
			<input name="product.pro_intro" type="hidden" id="pro_info" value="">
		</form>
	</div>
	<div class="row-fluid">
		<p class="sepH_c">
			<span class="label label-inverse">具体介绍</span>
		</p>
		<div class="span12">
			<script id="editor" type="text/plain"
				style="width: 100%; height: 500px;"></script>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span10">
		<button class="btn btn-info" id="enter" type="button">添加</button>
		<button class="btn" id="cancel" type="button">清空</button>
	</div>
	<div class="span2"></div>
</div>