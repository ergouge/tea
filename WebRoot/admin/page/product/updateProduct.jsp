<script>
window.UEDITOR_HOME_URL = "asset/ueditor/";
</script>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8" src="admin/lib/form/jquery.form.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.all.min.js"> </script>

<script type="text/javascript" charset="utf-8" src="asset/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
	var pro_id = "<%=request.getParameter("product_id")%>";
	var all_content="";
	var htmlContent;
	var news_title;
	$(document).ready(function(){
		$('#id_pro').val(pro_id);
	 	var all_content = "暂无内容";		
		//ueditor实例
		var ue = UE.getEditor('editor');
		ue.addListener("ready", function () {
		    // editor准备好之后才可以使用
		    ue.setContent(all_content);
		});
		
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {'menu.id':3},
			dataType : 'json',// 返回json格式的数据
			url : 'product/product!getProductTypes.sdo',// 要访问的后台地址
			success : function(data) {
				console.dir(data);
				if(data){			
					for(var i = 0; i < data.length; i ++) {
						$("#productType").append("<option value='"+data[i].id+"'>"+data[i].type_name+"</option>");
					}
				}else{
					$("#productType").append("<option>加载失败，请稍后重试</option>");
					$("#enter").attr("disabled", true);
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				
			}
		});
		
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {
				'product.id' : pro_id,
				},
			dataType : 'json',// 返回json格式的数据
			url : 'product/product!findProductById.action',// 要访问的后台地址
			success : function(data) {
				console.dir(data);
				$('#pro_num').val(data.pro_num);
				$('#pro_newPrice').val(data.pro_newPrice);
				$('#pro_oriPrice').val(data.pro_oriPrice);
				$('#pro_title').val(data.pro_title);
				$('#news_title').val(data.news_title);
				$('#pro_status').val(data.pro_status);
				$('#pro_TaoBaoURL').val(data.pro_TaoBaoURL);
				$('#pro_weiURL').val(data.pro_weiURL);
				$('#menu_id').val(data.menu.id);
				$('#pro_iconURL').attr('src',data.pro_iconURL);
				$('#productType').val(data.type.id);
				all_content=data.pro_intro;				
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				top.location.href="temp.jsp";
			}
		});
		
		
		/* $("#level_1").change(function(){
			$("#productType option").remove();
		 	if($("#level_1").val() == "绿茶") {
				$("#productType").append("<option value='1'>鸠坑毛尖</option>");
				$("#productType").append("<option value='2'>千岛玉叶</option>");
				$("#productType").append("<option value='3'>千岛银针</option>");
				$("#productType").append("<option value='4'>西湖龙井</option>");
			}
			if($("#level_1").val() == "红茶") {
				$("#productType").append("<option value='5'>鸠坑红茶</option>");
				$("#productType").append("<option value='6'>九曲红梅</option>");
				$("#productType").append("<option value='7'>普洱古树红茶</option>");
			}
			if($("#level_1").val() == "普洱") {
				$("#productType").append("<option value='8'>普洱云茶</option>");
				$("#productType").append("<option value='9'>古树生普</option>");
				$("#productType").append("<option value='10'>普洱熟茶</option>");
			}  
		}); */
	});
	
	//修改
	$("#enter").click(function (){
		/* if(!checkForm()){			
			return false;
		} */
		htmlContent = UE.getEditor('editor').getContent();
		$("#pro_info").val(htmlContent);
		var urlStr = 'product/product!updateProduct.action';
		//发送异步的请求
		$("#form1").ajaxSubmit({
			type : 'post',// 使用post方法访问后台
			async: false,
			dataType : 'json',// 返回json格式的数据
			url : urlStr,// 要访问的后台地址
			success : function(data) {
				if(data.result) {
					smoke.alert('修改成功.');
					$(".main_content").load("admin/page/product/product.jsp");
				} else {
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				top.location.href="page/error/error.jsp";
			}
		});
	})
     function checkForm(){
	    if(htmlContent==null||htmlContent==""){
		   smoke.alert('具体信息不能为空.');
		   return false;
	    }
	    if(news_title==null||news_title==""){
		   smoke.alert('不能为空.');
		   return false;
		}
		return true;
	}
</script>

<div class="row-fluid">
	<div class="span12">
		<form id="form1" enctype="multipart/form-data">
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">产品名称</span>
						</p>
						<input type="text" name="product.pro_title" id="pro_title"/>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">选择产品图片（最佳分辨率为200*150）</span>
						</p>
						<div data-provides="fileupload" class="fileupload fileupload-new">
							<input type="hidden">
							<div style="width: 200px; height: 150px;"
								class="fileupload-new thumbnail">
								<img src="admin/img/200x150/EFEFEF/AAAAAA&text=no+image.gif"
									alt="" id="pro_iconURL" />
							</div>
							<div
								style="max-width: 200px; max-height: 150px; line-height: 0px;"
								class="fileupload-preview fileupload-exists thumbnail"></div>
							<div>
								<span class="btn btn-file"><span class="fileupload-new">选择图片</span><span
									class="fileupload-exists">更换</span> <input type="file"
									name="productImg" /> </span> <a data-dismiss="fileupload"
									class="btn fileupload-exists" href="#">移除</a>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">一级类别</span>
						</p>
						<select name="productType.id" id="productType">
							
						</select>
					</div>
				<!-- 	<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">二级类别</span>
						</p>
						<select name="productType.id" id="productType">
							<option value="1">鸠坑毛尖</option>
							<option value="2">千岛玉叶</option>
							<option value="3">千岛银针</option>
							<option value="4">西湖龙井</option>
							<option value="5">鸠坑红茶</option>
							<option value="6">九曲红梅</option>
							<option value="7">普洱古树红茶</option>
							<option value="8">普洱云茶</option>
							<option value="9">古树生普</option>
							<option value="10">普洱熟茶</option>
						</select>
					</div> -->
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">产品原价</span>
						</p>
						<input type="text" name="product.pro_oriPrice" id="pro_oriPrice"/>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">产品现价</span>
						</p>
						<input type="text" name="product.pro_newPrice" id="pro_newPrice" />
					</div>
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">

						<p class="sepH_c">
							<span class="label label-inverse">产品状态</span>
						</p>
						<select name="product.pro_status" id="pro_status">
							<option value="新品">新品</option>
							<option value="热销">热销</option>
							<option value="下架">下架</option>
						</select>
					</div>
					<div class="span8">

						<p class="sepH_c">
							<span class="label label-inverse">产品数量</span>
						</p>
						<input type="text" name="product.pro_num" id="pro_num"/>
					</div>
				</div>
			</div>
			<div class="formSep">
				<div class="row-fluid">
					<div class="span4">
						<p class="sepH_c">
							<span class="label label-inverse">淘宝店对应商品网址</span>
						</p>

						<textarea name="product.pro_TaoBaoURL" id="pro_TaoBaoURL" cols="30"  
							rows="3"></textarea>
					</div>
					<div class="span8">
						<p class="sepH_c">
							<span class="label label-inverse">微店对应商品网址</span>
						</p>
						<textarea name="product.pro_weiURL" id="pro_weiURL" cols="30" 
							rows="3"></textarea>
					</div>
				</div>
			</div>
			<input type="hidden" value="" id="id_pro" name="product.id"/>
			<input type="hidden" value="12" id="menu_id" name="menu.id"/>
			<input name="product.pro_intro" type="hidden" id="pro_info" value="">
		</form>
	</div>
	<div class="row-fluid">
		<p class="sepH_c">
			<span class="label label-inverse">具体介绍</span>
		</p>
		<div class="span12">
			<script id="editor" type="text/plain"
				style="width: 100%; height: 500px;"></script>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span10">
		<button class="btn btn-info" id="enter" type="button">添加</button>
		<button class="btn" id="cancel" type="button">清空</button>
	</div>
	<div class="span2"></div>
</div>