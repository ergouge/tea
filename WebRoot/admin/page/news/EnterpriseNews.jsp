<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript"
	src="admin/javascript/userDefine/newsManager.js"></script>
<link rel="stylesheet" href="admin/bootstrap/css/font-awesome.min.css"
	type="text/css" />
<link rel="stylesheet" href="admin/bootstrap/css/pagination.css"
	type="text/css" />
<script type="text/javascript"
	src="admin/bootstrap/js/jquery.pagination.js"></script>
<style type="text/css">
.nodata {
	font-size: 20px;
	font-weight: bolder;
	padding-left: 10px;
	color: red;
	text-align: center;
}
</style>
<script type="text/javascript">


function newsDetail(news_id){
	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		data: {
			'news.id' : news_id
			},
		dataType : 'json',// 返回json格式的数据
		url : 'news/news!findNewsById.action',// 要访问的后台地址
		success : function(data) {
			$(".modal-title").html(data.news_title);
			$(".modal-body").html(data.news_content);
			$("#myModal").modal(); 
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}	
	});
}

 function  deleteNew(news_id){
		smoke.confirm("是否需要删除该新闻?", function(e){
	if (e){

	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		dataType : 'json',// 返回json格式的数据
		data: {
			
			'news.id' : news_id
			},
		url : 'news/news!deleteNewsById.action',// 要访问的后台地址
		success : function(data) {
			smoke.alert(data.msg, function(e){});
			$(".main_content").load("admin/page/news/EnterpriseNews.jsp");
		},	
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
		});
	}else{

	}
}, {
	ok: "删除",
	cancel: "取消",
	classname: "custom-class",
	reverseButtons: true

});
}; 

function deleteNews(){
	var news_id="";
	for(var i=0;i<$(".news_id").length;i++){
		if($(".news_id:eq("+i+")").attr("checked")=="checked"){
			news_id+=$(".news_id:eq("+i+")").val()+",";
		}
	}
	if(news_id==""){
	smoke.alert("请选择要删除的新闻", function(e){
	
}, {
	ok: "确定",
	classname: "custom-class"
});
	}else{

smoke.confirm("是否需要删除这些新闻?", function(e){
	if (e){

	
		$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		dataType : 'json',// 返回json格式的数据
		data: {
			'ids' : news_id
			},
		url : 'news/news!deleteNews.action',// 要访问的后台地址
		success : function(data) {
			/* console.dir(data); */                              
			smoke.alert(data.msg, function(e){});
			$(".main_content").load("admin/page/news/EnterpriseNews.jsp");
		},	
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
		});
	}else{

	}
}, {
	ok: "删除",
	cancel: "取消",
	classname: "custom-class",
	reverseButtons: true

});

	}
};
</script>
<input type="hidden" value="9" id="menu_id" />
<input type="hidden" value="${applicationScope.globleSize} "
	id="perPage" />
<div class="row-fluid">
	<div class="span3">
		<div class="btn-group">
			<a id="addNews" class="btn btn-success" href="javascript:void(0)"><i
				class="splashy-add"></i> 添加新闻</a> <a class="btn btn-danger"
				href="javascript:deleteNews()"><i class="splashy-remove_minus_sign"></i>
				删除新闻</a>
		</div>
	</div>
	<div class="span9 " align="right">
		<form action="" class="form-inline">
			<div class="form-group">
				<input type="text" style="width: 120px;" placeholder="请输入新闻标题"
					id="search_title"> <input type="text" style="width: 120px;"
					placeholder="请输入新闻内容" id="search_content"> <input
					type="text" style="width: 140px;" placeholder="请输入添加人员姓名"
					id="search_author">
				<button class="btn btn-info" id="search" type="button">
					<i class="splashy-zoom"></i>搜索
				</button>
			</div>
		</form>
	</div>
</div>
<div class="row-fluid" style="margin-top: 0px;">
	<div class="span12">
		<table
			class="table table-striped table-bordered   dataTable dataTables_wrapper"
			id="dt_a">
			<thead>
				<tr>
					<th class="center"><input id="selectAll" type="checkbox"></th>
					<th class="center">ID</th>
					<th class="center">新闻标题</th>					
					<th class="center">新闻添加人员</th>
					<th class="center">新闻添加时间</th>
					<th class="center">新闻最后修改时间</th>
					<th class="center">新闻浏览次数</th>
					<th class="center" colspan="3" style="width: 260px;">动作</th>
				</tr>
			</thead>
			<tbody id="newsList">
			</tbody>
		</table>
	</div>
</div>
<!--分页开始-->
<div class="row-fluid" id="pageNum" style="margin-top: 10px">
	<div class="span12">
		<div class="pagination"
			style="float: right; margin: 0px 20px 0px 0px;">
			<div style="float: left;">
				<ul id="Pagination">
				</ul>
			</div>
			<div
				style="vertical-align: top; height: 30px; width: auto; float: left;">
				<input id="currentPage" type="text"
					style="width: 38px; height: 30px; margin-left: 5px; margin-top:1px;text-align: center" />
				<label id="numPage"
					style="color: darkgrey; font-size: 15px; display: inline-block;"></label>
				<button id="goPage"
					style="width: 55px; height: 29px; vertical-align: top;"
					class="btn btn-small btn-primary">跳转</button>
			</div>
		</div>
	</div>
</div>
<!--分页结束-->
<div class="row-fluid" id="pageNum2"></div>
<div class="modal fade " id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">新闻标题</h4>
			</div>
			<div class="modal-body">
				<p>这里是弹框的具体内容</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary">保存</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var perPage =  $("#perPage").val(); 
	var title="" ;
	var content="" ;
	var author="" ;
	
$("#search").click(function(){
var optInit = getOptionsFromForm();
	total = (function(){
	var result;
	title = $("#search_title").val();
	content = $("#search_content").val();
	author = $("#search_author").val();
	$.ajax({
		
        type : "POST",
        dataType : "json",
        async : false,
        data : {
        	start : 0,
        	limit : 0,'menu.id' : 
        	$('#menu_id').val(),
        	'news.news_title':title,
    		'news.news_content':content,
    		'news.user.user_name':author
        },
        url : 'news/news!getNewsList.action',
        success : function(msg) {
        	result = msg.total;
        }
	});
	return result;
})();
	//赋值显示 页码信息
    $("#Pagination").pagination(total, optInit);
})	;


	
	var total = (function() {
	var result;
	/*title = $("#search_title").val();
	content = $("#search_content").val();
	author = $("#search_author").val();*/
	$.ajax({	
		        type : "POST",
                dataType : "json",
                async : false,
                data : {
                	start : 0,
                	limit : 0,'menu.id' : $('#menu_id').val(),
                	'news.news_title':title,
            		'news.news_content':content,
            		'news.user.user_name':author
                },
                url : 'news/news!getNewsList.action',
                success : function(msg) {
                	result = msg.total;
                }
            });
            return result;
        })();
	var gPage_index = 0;
	var vPage_index = 0;
	function pageselectCallback(page_index, jq){
	    var items_per_page = perPage;//每页显示的内容数量
	    var max_elem = Math.min((page_index+1) * items_per_page, total);
	    function numPages() {
	        return Math.ceil(total/items_per_page);
	    }
	    $("#currentPage").val(page_index+1);
	    gPage_index = page_index+1;
	    $("#numPage").html("/"+numPages(total)+"页");
	    /*title = $("#search_title").val();
		content = $("#search_content").val();
		author = $("#search_author").val();*/
	    $.ajax({
	        type : "POST",
	        dataType : "json",
	        async : false,
	        url : 'news/news!getNewsList.action', //后台数据提取
	        data : {start:page_index * items_per_page,limit:items_per_page,'menu.id' : $('#menu_id').val(),
	        	'news.news_title':title,
	    		'news.news_content':content,
	    		'news.user.user_name':author},
	        success : function(msg) {
	        	 if (msg.rows.length > 0) {
	        	   $("#newsList").empty();
	               //var dataObj=eval("("+data+")");//转换为json对象
	               $.each(msg.rows,function(index,data){
	            	$("#pageNum").show();
					$("#pageNum2").hide();
	            	var content ="<tr><td  style='text-align: center;'><input type='checkbox' value='"+data.id+"' name='chkItem' class='news_id'/></td>"
					+"<td style='text-align: center;'>"+data.id+"</td>"
					+"<td style='text-align: center;'>"+data.news_title+"</td>"					
					+"<td style='text-align: center;'>"+data.user.user_name+"</td>"
					+"<td style='text-align: center;'>"+data.news_createTime+"</td>"
					+"<td style='text-align: center;'>"+data.news_modifyTime+"</td>"
					+"<td style='text-align: center;'>"+data.news_viewTimes+"</td>"
					+"<td style='text-align: center;'><a class='btn btn-success btn-small'  href='javascript:newsDetail("+data.id+")' ><i class='splashy-view_list'></i>详情</a></td>"
					+"<td style='text-align: center;'><a class='btn btn-info btn-small' href='javascript:void(0)'><i class='splashy-sprocket_light'></i>修改</a></td>"
					+"<td style='text-align: center;'><a class='btn btn-danger btn-small' href='javascript:deleteNew("+data.id+")'><i class='splashy-error_x'></i> 删除</a></td>"
					+"</tr>";
					$("#newsList").append(content);
	               });
	             }
	        	 else {
	        		 $("#pageNum").hide();
					 $("#pageNum2").show();
	        		 $("#newsList").empty();
	        		 $("#pageNum2").html("<span class='nodata' >请重新输入查询关键词，你所查询的关键词无内容！</span>");
					}
	           }
	       });
	    return false;
	}
        //设置基本参数
        function getOptionsFromForm(){
            var opt = {callback: pageselectCallback};
            opt.items_per_page = parseInt(perPage);  //每页中的内容行数
            opt.num_display_entries = parseInt("5"); //分页中央显示页码数量
            opt.num_edge_entries = parseInt("1");     //分页两侧显示页码数量
            opt.prev_text = "<<";
            opt.next_text = ">>";
            return opt;
        }
        //设置基本参数
        $(document).ready(function(){
        	$("#addNews").click(function(){
        		$(".main_content").load("admin/page/news/addIndustryNews.jsp");
        	});
            var optInit = getOptionsFromForm();
            $("#Pagination").pagination(total, optInit);
            $("#goPage").click(function(){
                var opt = getOptionsFromForm();
                var currentPage = $("#currentPage").val();
                var maxPage = Math.ceil(total/opt.items_per_page);
                if(currentPage>maxPage || currentPage<0){
                    alert("请输入范围内的页码");
                    $("#currentPage").val(gPage_index);
                }else{
                    opt.current_page = currentPage-1;
                    $("#Pagination").pagination(total, opt);
                }
            });
        });
        $("#selectAll").click(function(){
		if($(this).attr("checked") == "checked"){
			$(".news_id").attr("checked","checked");
		}else{
			$(".news_id").removeAttr("checked");
		}
	});
</script>