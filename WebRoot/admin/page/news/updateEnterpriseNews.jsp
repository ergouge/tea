<script>
window.UEDITOR_HOME_URL = "asset/ueditor/";
</script>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="asset/ueditor/ueditor.all.min.js"> </script>

<script type="text/javascript" charset="utf-8" src="asset/ueditor/lang/zh-cn/zh-cn.js"></script>
<script src="js/gebo_notifications.js" ></script>

<script type="text/javascript">
	var news_id = "<%=request.getParameter("news_id")%>";
	var htmlContent ;
	var news_title;
	$(document).ready(function(){
		var all_content = "暂无内容";		
		//ueditor实例
		var ue = UE.getEditor('editor');
		ue.addListener("ready", function () {
		    // editor准备好之后才可以使用
		    ue.setContent(all_content);
		});
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {
				'news.id' : news_id,
				},
			dataType : 'json',// 返回json格式的数据
			url : 'news/news!findNewsById.action',// 要访问的后台地址
			success : function(data) {
					$('#news_title').val(data.news_title);
					all_content=data.news_content;				
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	});
	
	//修改
	$("#enter").click(function update(){
		htmlContent = UE.getEditor('editor').getContent();
		news_title = $("#news_title").val();
		var urlStr = 'news/news!updateNews.action';
		if(!checkForm()){			
			return false;
		}
		//发送异步的请求
		$.ajax({
			type : 'post',// 使用post方法访问后台
			async: false,
			data: {
			    'news.id':news_id,
				'news.news_title' :news_title ,
				'menu.id':$("#menu_id").val(),
				'news.news_content': htmlContent,
				},
			dataType : 'json',// 返回json格式的数据
			url : urlStr,// 要访问的后台地址
			success : function(data) {
				if(data.result) {
					smoke.alert('新闻修改成功.');
					$(".main_content").load("admin/page/news/EnterpriseNews.jsp");
				} else {
					
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert(errorThrown);
			}
		});
	})
    function checkForm(){
	    if(htmlContent==null||htmlContent==""){
		   smoke.alert('新闻内容不能为空.');
		   return false;
	    }
	    if(news_title==null||news_title==""){
		   smoke.alert('新闻标题不能为空.');
		   return false;
		}
		return true;
	}
</script>
<input type="hidden" value="9" id="menu_id"/>
<div class="row-fluid">
	<div class="span12">
				<p class="f_legend">修改新闻</p>
				<div class="span8">
					<div class="control-group">
						<label class="control-label">新闻标题</label>
						<div class="controls">
							<input type="text"  id="news_title"/> 
						</div> 
					</div>
					</div>
					<div class="span11">
					<div class="control-group">
						<label class="control-label">新闻内容 </label>
						<div class="controls">
							<script id="editor" type="text/plain" style="width:100%; height: 500px;"></script>
							<!-- <span class="help-block">block help text</span> -->
						</div>
					</div>
					<div class="control-group">
						<div class="row-fluid">
						 <button class="btn btn-info" id="enter">修改</button>
						 <button class="btn" id="cancel">取消</button>
					  </div>
					</div>
				</div>		
	</div>
</div>