<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript">
	function update(id,user_name,user_email,user_phone,user_pwd,user_gender,user_address,user_age){
		var flag = false;
		$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		data: {
			'user.user_name' : user_name,
			'user.id' : id,
			'user.user_email' : user_email,
			'user.user_phone' : user_phone,
			'user.user_pwd' : user_pwd,
			'user.user_gender' : user_gender,
			'user.user_adress' : user_address,
			'user.user_age':user_age
			},
		dataType : 'json',// 返回json格式的数据
		url : 'user/user!updateUserById.action',// 要访问的后台地址
		success : function(data) {
			if(data.result){
				flag = true;
				smoke.alert(data.msg, function(e){
	
}, {
	ok: "确定",
	classname: "custom-class"
});
			}else{
				flag = false;
			}
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});
		if(flag){
			return true;
		}else{
			return false;
		}
	}
	$(document).ready(function(){
		var user_gender = "${userInfo.user_gender}";
		if(user_gender == "男"){
			$("input[name='user_gender']:eq(0)").attr("checked","checked");
		}else{
			$("input[name='user_gender']:eq(1)").attr("checked","checked");
		}
		$("#changeUserInfo").click(function(){
			var user_name = $("input[name='user.user_name']").val();
			var id = $("input[name='user.id']").val();
			var user_email = $("input[name='user.user_email']").val();
			var user_phone = $("input[name='user.user_phone']").val();
			var user_pwd = $("input[name='user.user_pwd']").val();
			var repeat_pwd = $("input[type='password']:eq(1)").val();
			if(user_pwd!=repeat_pwd){
					smoke.alert("修改密码不一致！", function(e){
	
}, {
	ok: "确定",
	classname: "custom-class"
});
					return;
			}	 
			var user_gender =$("input[name='user_gender']:checked").val(); 
			var user_address = $("textarea[name='user_adress']").val();
			var user_age = $("input[name='user.user_age']").val();		
			if(update(id,user_name,user_email,user_phone,user_pwd,user_gender,user_address,user_age)){
				$(".main_content").load("user/user!findUserById.action?user.id=${ sessionScope.userSession.id}");
			};
			
		});
	});
</script>
<div class="row-fluid">
	<div class="span12">
		<h3 class="heading">我的信息</h3>
		<div class="row-fluid">
			<div class="span8">
				<form class="form-horizontal" >
					<fieldset>
						<div class="control-group formSep">
							<label class="control-label">登录账号</label>
							<div class="controls text_line" style="line-height: 30px;"> <strong>${userInfo.login_name}</strong>
							</div>
						</div>
						<div class="control-group formSep">
							<label class="control-label">用户角色</label>
							<div class="controls text_line" style="line-height: 30px;"> <strong>${userInfo.user_role}</strong>
							</div>
						</div>
						<div class="control-group formSep">
							<label class="control-label" for="u_fname">用户姓名</label>
							<div class="controls">
								<input type="text" name="user.user_name" value="${userInfo.user_name}" class="input-xlarge" id="u_fname">
								<input type="text" name="user.id" style="display: none;" value="${userInfo.id}" class="input-xlarge" id="u_fname"></div>
						</div>
						<div class="control-group formSep">
							<label class="control-label" for="u_email">用户邮箱</label>
							<div class="controls">
								<input type="text" name="user.user_email" value="${userInfo.user_email}" class="input-xlarge" id="u_email"></div>
						</div>
						<div class="control-group formSep">
							<label class="control-label" for="u_email">手机号码</label>
							<div class="controls">
								<input type="text" name="user.user_phone" value="${userInfo.user_phone}" class="input-xlarge" id="u_email"></div>
						</div><div class="control-group formSep">
							<label class="control-label" for="u_email">用户年龄</label>
							<div class="controls">
								<input type="text" name="user.user_age" value="${userInfo.user_age}" class="input-xlarge" id="u_email"></div>
						</div>
						<div class="control-group formSep">
							<label class="control-label" for="u_password">用户密码</label>
							<div class="controls">
								<div class="sepH_b">
									<input type="password" name="user.user_pwd" value="" class="input-xlarge" id="u_password">
									<span class="help-block">Enter your password</span>
								</div>
								<input type="password" class="input-xlarge" id="s_password_re">
								<span class="help-block">Repeat password</span>
							</div>
						</div>
						<div class="control-group formSep">
							<label class="control-label">性别</label>
							<div class="controls">
								<label class="radio inline">
									<input type="radio" checked="checked" name="user_gender" id="s_malse" value="男">男</label>
								<label class="radio inline">
									<input type="radio" name="user_gender" id="s_female" value="女">女</label>
							</div>
						</div>
						<div class="control-group formSep">
							<label class="control-label" for="u_signature">用户地址</label>
							<div class="controls">
								<textarea class="input-xlarge" id="u_signature" name="user_adress" rows="4" style="overflow: hidden; word-wrap: break-word; max-height: 80px; min-height: 80px; height: 80px;"> ${userInfo.user_adress }</textarea>
								<span class="help-block">Automatic resize</span>
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<button type="button" id="changeUserInfo" class="btn btn-gebo">修改信息</button>
								<button class="btn">取消</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>