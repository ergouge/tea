<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<div class="style_switcher">
		<div class="sepH_c">
			<p>颜色:</p>
			<div class="clearfix">
				<a href="javascript:void(0)"
					class="style_item jQclr blue_theme style_active" title="blue">蓝色</a>
				<a href="javascript:void(0)" class="style_item jQclr dark_theme"
					title="dark">黑色</a> <a href="javascript:void(0)"
					class="style_item jQclr green_theme" title="green">绿色</a> <a
					href="javascript:void(0)" class="style_item jQclr brown_theme"
					title="brown">棕色</a> <a href="javascript:void(0)"
					class="style_item jQclr eastern_blue_theme" title="eastern_blue">东方蓝
					</a> <a href="javascript:void(0)"
					class="style_item jQclr tamarillo_theme" title="tamarillo">番茄色</a>
			</div>
		</div>
		<div class="sepH_c">
			<p>背景:</p>
			<div class="clearfix">
				<span class="style_item jQptrn style_active ptrn_def" title=""></span>
				<span class="ssw_ptrn_a style_item jQptrn" title="ptrn_a"></span> <span
					class="ssw_ptrn_b style_item jQptrn" title="ptrn_b"></span> <span
					class="ssw_ptrn_c style_item jQptrn" title="ptrn_c"></span> <span
					class="ssw_ptrn_d style_item jQptrn" title="ptrn_d"></span> <span
					class="ssw_ptrn_e style_item jQptrn" title="ptrn_e"></span>
			</div>
		</div>
		<div class="sepH_c">
			<p>菜单栏 位置:</p>
			<div class="clearfix">
				<label class="radio inline"><input type="radio"
					name="ssw_sidebar" id="ssw_sidebar_left" value="" checked /> 左</label>
				<label class="radio inline"><input type="radio"
					name="ssw_sidebar" id="ssw_sidebar_right" value="sidebar_right" />
					右</label>
			</div>
		</div>
		<div class="sepH_c">
			<p>顶部菜单显示:</p>
			<div class="clearfix">
				<label class="radio inline"><input type="radio"
					name="ssw_menu" id="ssw_menu_click" value="" checked /> 点击</label> <label
					class="radio inline"><input type="radio" name="ssw_menu"
					id="ssw_menu_hover" value="menu_hover" /> 悬停</label>
			</div>
		</div>

		<div class="gh_button-group">
			<a href="#" id="showCss" class="btn btn-primary btn-mini">显示
				CSS</a> <a href="#" id="resetDefault" class="btn btn-mini">重置</a>
		</div>
		<div class="hide">
			<ul id="ssw_styles">
				<li class="small ssw_mbColor sepH_a" style="display:none">body
					{<span class="ssw_mColor sepH_a" style="display:none">
						color: #<span></span>;</span> <span class="ssw_bColor"
					style="display:none">background-color: #<span></span> </span>}</li>
				<li class="small ssw_lColor sepH_a" style="display:none">a {
					color: #<span></span> }</li>
			</ul>
		</div>
	</div>