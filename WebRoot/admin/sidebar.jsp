<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
	ul li a{
		cursor: pointer;
	}
</style>
	<div class="sidebar">

			<div class="antiScroll">
				<div class="antiscroll-inner">
					<div class="antiscroll-content">

						<div class="sidebar_inner">
							<form
								action="http://tzd-themes.com/gebo_admin/index.php?uid=1&page=search_page"
								class="input-append" method="post">
								<input autocomplete="off" name="query"
									class="search_query input-medium" size="16" type="text"
									placeholder="Search..." />
								<button type="submit" class="btn">
									<i class="icon-search"></i>
								</button>
							</form>
							<div id="side_accordion" class="accordion">

								<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseOne" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="splashy-zoom"></i> 走进 </a>
									</div>
									<div class="accordion-body collapse" id="collapseOne">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li class="text-tumed">
											<!-- 	<a href="admin/page/zouJingJiuKen/Introduce.jsp">企业简介</a> -->
												<a  menu_id="3" >企业简介</a>
												</li>
												<li><a menu_id="4">鸠坑茶历史</a>
												</li>
												<li><a menu_id="5">企业文化</a>
												</li>
												<li><a menu_id="6">领导关怀</a>
												</li>
												<li><a menu_id="7">企业荣誉</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseTwo" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="icon-th"></i> 新闻 </a>
									</div>
									<div class="accordion-body collapse" id="collapseTwo">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li><a menu_id="9">企业播报</a>
												</li>
												<li><a menu_id="10">行业动态</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseThree" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="splashy-diamonds_4"></i> 产品  </a>
									</div>
									<div class="accordion-body collapse" id="collapseThree">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li><a menu_id="11">产品</a>
												</li>												
											</ul>

										</div>
									</div>
								</div>
								<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseFour" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="icon-user"></i> 会员专区 </a>
									</div>
									<div class="accordion-body collapse" id="collapseFour">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li class=""><a menu_id="15">VIP 优惠</a></li>
												<li ><a menu_id="27">
														会员活动</a>
												</li>
												<li><a menu_id="16">会员公告  </a>
												</li>
												<li class=""><a menu_id="17">会员资料</a></li>
												<li><a menu_id="18">会员申请 </a>
												</li>
											
											</ul>
										</div>
									</div>
								</div>
								
							<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseFive" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="splashy-marker_rounded_grey_4"></i> 营销网络 </a>
									</div>
									<div class="accordion-body collapse" id="collapseFive">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li ><a menu_id="20">直营店</a></li>
												<li ><a menu_id="21">
														微信商城</a>
												</li>
												<li><a menu_id="22">淘宝商城  </a>
												</li>
												
											
											</ul>
										</div>
									</div>
								</div>
							<div class="accordion-group">
									<div class="accordion-heading">
										<a href="#collapseSix" data-parent="#side_accordion"
											data-toggle="collapse" class="accordion-toggle"> <i
											class="splashy-contact_grey_edit"></i> 联系我们 </a>
									</div>
									<div class="accordion-body collapse" id="collapseSix">
										<div class="accordion-inner">
											<ul class="nav nav-list">
												<li ><a menu_id="24">人才招聘</a></li>
												<li class=""><a menu_id="25">
														加盟在线申请</a>
												</li>
												<li><a menu_id="26">联系方式 </a>
												</li>
												
											
											</ul>
										</div>
									</div>
								</div>

							</div>

							<div class="push"></div>
						</div>

					<!-- 	<div class="sidebar_info">
							<ul class="unstyled">
								<li><span class="act act-warning">65</span> <strong>New
										comments</strong></li>
								<li><span class="act act-success">10</span> <strong>New
										articles</strong></li>
								<li><span class="act act-danger">85</span> <strong>New
										registrations</strong></li>
							</ul>
						</div> -->

					</div>
				</div>
			</div>

		</div>