<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>淳安县鸠坑万岁岭茶叶专业合作社</title>
<link href="asset/css/style.css" rel="stylesheet" type="text/css" />
<link href="asset/css/index.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="asset/css/zzsc.css" type="text/css" media="screen" />
<script type="text/javascript" src="asset/js/jquery.min.js"></script>
<script type="text/javascript" src="asset/js/zzsc.js"></script>
<link href="asset/css/style1.css" rel="stylesheet" type="text/css" />
<link href="asset/css/luara.left.css" rel="stylesheet" type="text/css" /> 
<link rel=stylesheet type=text/css href="asset/css/kefu.css">
<link rel="shortcut icon" href="<%=basePath%>/asset/images/favicon.ico" type="image/x-icon" />
</head>

<body style="background-color:#ECECEC">


	<!-- 头部 -->
	<%@include file="header.jsp"%>
	<div id="main" style="height: auto; width: 1000px; margin: 0 auto; margin-top: -30px;">
	<div id="main_content1"  >
	<div id="header">
  <div class="wrap">
   <div id="slide-holder">
		<div id="slide-runner">
			<a href="#" target="_blank"><img height="400px" width="1000px" id="slide-img-1" src="asset/images/home/3.png" class="slide" alt="" /></a>
			<a href="#" target="_blank"><img height="400px" width="1000px" id="slide-img-2" src="asset/images/home/1.png" class="slide" alt="" /></a>
			<a href="#" target="_blank"><img height="400px" width="1000px" id="slide-img-3" src="asset/images/home/2.png" class="slide" alt="" /></a>
			<div id="slide-controls">
			 <p id="slide-client" class="text"><strong></strong><span></span></p>
			 <p id="slide-desc" class="text"></p>
			 <p id="slide-nav"></p>
			</div>
		</div>
   </div>
   <script type="text/javascript">
    if(!window.slider) {
		var slider={};
	}
    slider.data= [
                  {
                      "id":"slide-img-1", // 与slide-runner中的img标签id对应
                      "client":"",
                      "desc":"" //这里修改描述
                  },
                  {
                      "id":"slide-img-2",
                      "client":"",
                      "desc":""
                  },
                  {
                      "id":"slide-img-3",
                      "client":"",
                      "desc":""
                  }
              	];
   </script>
  </div>
</div>
	
	
	</div>
	</div>
	
		<div id="main_content2">
			<!-- 左侧项目div开始 -->
			<div id="content_left"  >
				<div class="news">
					<div class="news_title1">
						新闻中心
					</div>
					<div class="news_content" >
						<ul id="newsList">
						</ul>

					</div>

				</div>
			</div>
			<!-- 左侧项目div开始 -->
			<!-- 中间新闻div开始 -->
			<div id="content_center" >
				<div class="news">
					<div class="news_title1" >
						公司简介
					</div>
					<div class="cop_intro">
						<div  class="cop_image"><img width="100%" height="100%" src="asset/images/1.gif"/></div>
						<div  class="cop_content">
							<h2 align="center"  ><a href="./page/zouJingJiuKen/Introduce.jsp" target="_blank">淳安县鸠坑万岁岭茶叶专业合作社</a></h2>
							<a href="./page/zouJingJiuKen/Introduce.jsp" target="_blank">
							<div id="intro_content">
						
							</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- 中间新闻div结束 -->
			<!-- 右侧项目div开始 -->
			<div id="content_right" >
				<div class="news">
					<div class="news_title1">
						营销网络
					</div>
					<div class="news_content">
					<div class="button_img_div"><img src="asset/images/zhiying.jpg"/></div>	
					<a href=""><div class="button_div" >直营店</div>	</a>
					<div class="button_img_div"><img src="asset/images/weixin.jpg"/></div>						
					<a href=""><div class="button_div" >微信商城</div></a>
					<div class="button_img_div"><a href="http://shop104452439.taobao.com/?spm=a230r.7195193.1997079397.2.py3QU4" target="_blank"><img src="asset/images/taobao.jpg"/></a></div>	
					<a href="http://shop104452439.taobao.com/?spm=a230r.7195193.1997079397.2.py3QU4" target="_blank"><div class="button_div" >淘宝店</div></a>
					
				</div>
			</div>
			<!-- 右侧项目div开始 -->
			</div>
		</div>
		
		<div style="clear:both;">
		</div>
	
		<div  style="height: 200px; width: 1000px; margin: 0 auto;background-color:#ffffff">
			<div class="news_title1">产品展示</div>
 <!--下面是向左滚动代码-->
<div id="colee_left" style="overflow:hidden;width:980px;margin-left:10px;padding-top:10px">
<table cellpadding="0" cellspacing="0" border="0">
<tr><td id="colee_left1" valign="top" align="center">
<table cellpadding="2" cellspacing="0" border="0">
<tr align="center" id="shili">
<!-- <td style="padding:5px"><a href="#" id ="product1"><div><img src="" style="border:2px solid #BFBFBF" ></div></a></td>
<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片2</a></td>
<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片3</a></td>
<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片4</a></td>
<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片5</a></td>
<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片6</a></td> -->
</tr>
</table>
</td>
<td id="colee_left2" valign="top" align="center"></td>
</tr>
</table>
</div>


			
		</div>
		<div id=chat_f1 style="margin-top: 10px;">
			<div id=chat_f1_main>
				<div id=close></div>
			</div>
			<div class=chat_f1_expr>
				<div class=list>
					<div class=name>
						&nbsp;<span class=arrow>•</span>&nbsp;售前咨询
						<div class=detail>
							<div class=border></div>
							<div class=mt>
								<strong>竭诚为您提供：</strong><br>● 购买指导<br>● 选购建议<br>●
								价格优惠
							</div>
							<div class=mt>● 服务热线：0571-86957651</div>
							<div class="mt mt5">
								<A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845297419&amp;site=qq&amp;menu=yes"
									target=_blank><IMG title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A> <A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845297419&amp;site=qq&amp;menu=yes"
									target=_blank><IMG title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A>
							</div>
						</div>
					</div>
				</div>
				<div class=list>
					<div class=name>
						&nbsp;<span class=arrow>•</span>&nbsp;技术支持
						<div class=detail>
							<div class=border></div>
							<div class=mt>
								<strong>竭诚为您提供：</strong><br>● 技术支持<br>● 更新升级<br>●
								后期维护
							</div>
							<div class=mt>● 服务热线：18782998174</div>
							<div class="mt mt5">
								<A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845371184&amp;site=qq&amp;menu=yes"
									target=_blank><img title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A> <A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845371184&amp;site=qq&amp;menu=yes"
									target=_blank><img title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A>
							</div>
						</div>
					</div>
				</div>
				<div class=list>
					<div class=name>
						&nbsp;<span class=arrow>•</span>&nbsp;业务合作
						<div class=detail>
							<div class=border></div>
							<div class=mt>
								<strong>竭诚为您提供：</strong><br>● 合作咨询  <br>●  业务咨询<br>●
								 业务合作
							</div>
							<div class=mt>● 服务热线：0571-86957651</div>
							<div class="mt mt5">
								<A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845297419&amp;site=qq&amp;menu=yes"
									target=_blank><img title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A> <A onfocus=this.blur()
									href="http://wpa.qq.com/msgrd?v=3&amp;uin=845297419&amp;site=qq&amp;menu=yes"
									target=_blank><img title=点击这里给我发消息 border=0 alt=鸠坑毛尖客服QQ
									src="asset/images/button_111.jpg">
								</A>
							</div>
						</div>
					</div>
				</div>
				<div class=list>
					<div class=name>
						&nbsp;<span class=arrow>•</span>&nbsp;淘宝店铺
						<div class=detail>
							<div class=border></div>
							<div class=mt>
								<img alt=鸠坑毛尖官方淘宝店铺 src="asset/images/taobao_guanzhu.png" width=190 height=200>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id=chat_f1_bottom></div>
		</div>
		<div style="DISPLAY: none" id=chat_f2>在线客服</div>
				<script type=text/javascript>
				$(document).ready(
					function() {
						$('.chat_f1_expr').animate({
							height : '104px'
						}, 1000);
						$('#close').click(function() {
							$('#chat_f1').hide();
							$('#chat_f2').show();
						})
						$('#chat_f2').click(function() {
							$(this).hide();
							$('#chat_f1').show();
						})
						$('.name').hover(function() {
							$(this).children('.detail').show();
							$(this).children('.arrow').css('color', '#a00');
						}, function() {
							$(this).children('.detail').hide();
							$(this).children('.arrow').css('color', '#fff');
						})

						var tips;
						var theTop = 125;
						var old = theTop;
						function initFloatTips() {
							tips = document.getElementById('chat_f1');
							moveTips();
						}
						;
						function moveTips() {
							var tt = 100;
							if (window.innerHeight) {
								pos = window.pageYOffset
							} else if (document.documentElement
									&& document.documentElement.scrollTop) {
								pos = document.documentElement.scrollTop
							} else if (document.body) {
								pos = document.body.scrollTop;
							}
							pos = pos - tips.offsetTop + theTop;
							pos = tips.offsetTop + pos / 10;
							if (pos < theTop)
								pos = theTop;
							if (pos != old) {
								tips.style.top = pos + "px";
								tt = 10;
							}
							old = pos;
							setTimeout(moveTips, tt);
						}
						initFloatTips();
					});
		</script>
		<script type=text/javascript src="asset/js/jquery.caroufredsel.js"></script>
		<!-- 底部 -->
	<%@include file="footer.jsp"%>
<script type="text/javascript">
$(document).ready(function(){
	/* 	var header = $("#intro_content");
		$clamp(header, {clamp: 1});	 */
	//获取新闻列表
	$.ajax({
		type : 'post',// 使用post方法访问后台
		async: false,
		data: {},
		dataType : 'json',// 返回json格式的数据
		url : 'news/news!getFrontPageNewsList.sdo',// 要访问的后台地址
		success : function(data) {
			if(data.length>0){
				$("#newsList").empty();
				$.each(data,function(index,data){
					var li="<li><div class='list_title'><a href='news/news!viewNews.sdo?news.id="+data.id+"' target='_blank'>"+data.news_title+"</a></div><div class='list_time'>"+data.news_createTime.substring(0,10)+"</div></li>"
	            	$("#newsList").append(li);
	               }); 
			}else{
				$("#newsList").empty();
				$("#newsList").append("暂无新闻。");
				
			}
		
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			
		}
	});
});
//获取公司简介
$.ajax({
	type : 'post',// 使用post方法访问后台
	async: false,
	data: {'menu.id':3},
	dataType : 'json',// 返回json格式的数据
	url : 'introduce/introduce!getOneObj.sdo',// 要访问的后台地址
	success : function(data) {

		if(data!=''){		
		 
			$("#intro_content").html(data.intro_content); 
			
		}else{
			
		}
	
	},
	error : function(XMLHttpRequest, textStatus, errorThrown) {
		
	}
});
//获取产品展示
$.ajax({
	type : 'post',// 使用post方法访问后台
	async: false,
	data: {'limit':6,'start':0},
	dataType : 'json',// 返回json格式的数据
	url : 'product/product!getProductList.sdo',// 要访问的后台地址
	success : function(data) {
		 if(data.rows.length>0){				 
			//$("#intro_content").html(data.intro_content);
			for(var i=0;i<data.rows.length;i++){
// 				<td style="padding:5px"><a href="#" ><div><img src="asset/images/product1.gif" style="border:2px solid #BFBFBF"></div>示例图片2</a></td>
				$("#shili").append("<td style='padding:5px'><a href='"+data.rows[i].pro_TaoBaoURL+"' ><div><img width='180px' height='112px' src='"+data.rows[i].pro_iconURL+"' style='border:2px solid #BFBFBF'></div></a></td>");
				//$("#product1").html(data.pro_title);
				//$("#product1").children("img").attr("src", data.pro_iconURL);
				
			}
		 }
	},
	error : function(XMLHttpRequest, textStatus, errorThrown) {
		
	}
});
</script>

</body>
</html>
<script>
//使用div时，请保证colee_left2与colee_left1是在同一行上.
var speed=30//速度数值越大速度越慢
/* var colee_left2=document.getElementById("colee_left2");
var colee_left1=document.getElementById("colee_left1");
var colee_left=document.getElementById("colee_left"); */
colee_left2.innerHTML=colee_left1.innerHTML
function Marquee3(){
if(colee_left2.offsetWidth-colee_left.scrollLeft<=0)//offsetWidth 是对象的可见宽度
colee_left.scrollLeft-=colee_left1.offsetWidth//scrollWidth 是对象的实际内容的宽，不包边线宽度
else{
colee_left.scrollLeft++
}
}
var MyMar3=setInterval(Marquee3,speed)
colee_left.onmouseover=function() {clearInterval(MyMar3)}
colee_left.onmouseout=function() {MyMar3=setInterval(Marquee3,speed)}
</script>