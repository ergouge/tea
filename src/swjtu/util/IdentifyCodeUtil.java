package swjtu.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

public class IdentifyCodeUtil
{
	public static final int WIDTH = 60;
	public static final int HEIGHT = 20;
	public static final int SIZE = 4;
	private String codeStr;

	public ByteArrayInputStream generteCode()
	{
		codeStr = RandomUtil.getRandeomString(SIZE);

		// 创建图片
		BufferedImage img = new BufferedImage(WIDTH, HEIGHT,
				BufferedImage.TYPE_INT_RGB);
		// 获取图片上下文
		Graphics grap = img.getGraphics();

		// 设置填充区
		grap.setColor(Color.white);
		grap.fillRect(0, 0, WIDTH, HEIGHT);

		// 添加干扰线
		
		Random random = new Random();
		for (int i = 0; i < 10; i++)
		{
			int x = random.nextInt(WIDTH);
			int y = random.nextInt(HEIGHT);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			grap.setColor(new Color(255));
			grap.drawLine(x, y, x + xl, y + yl);
		}

		// 填充字符串到填充区
		grap.setFont(new Font(null, Font.PLAIN, 18));
		grap.setColor(Color.black);
		grap.drawString(codeStr, 10, 17);

		System.out.println(codeStr);

		// 完成
		grap.dispose();

		ByteArrayInputStream input = null;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try
		{
			//设置输出目标为：output
			ImageOutputStream imageOut = ImageIO
					.createImageOutputStream(output);
			//把 img对象 写入到 imageOut中
			ImageIO.write(img, "JPEG", imageOut);
			imageOut.close();
			//获取输出流对象 返回
			input = new ByteArrayInputStream(output.toByteArray());
		} catch (Exception e)
		{
			System.out.println("产生验证码出错：" + e.toString());
		}

		// 返回输出流
		return input;
		

	}

	public String getCodeStr()
	{
		return codeStr;
	}

	public void setCodeStr(String codeStr)
	{
		this.codeStr = codeStr;
	}
}
