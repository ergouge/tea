/**
 * created on 2014年10月7日
 * Author ergouge
 * Email ergouge@gmail.com
 * All rights reserved
 */
package swjtu.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;
import swjtu.model.User;

/**
 * TODO
 * @author ERGOUGE
 * 2014年10月7日 下午6:35:56
 */
public class MyStrutsFilter extends StrutsPrepareAndExecuteFilter{
	public void doFilter(ServletRequest req, ServletResponse res,FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper((HttpServletResponse) res);
        String url = request.getRequestURI();
        String rootPath = request.getContextPath();
        System.out.println(url);
        if(url.contains("admin")) {
            User user = (User) request.getSession().getAttribute("userSession");//判断用户是否登录
            if(user == null) {
                wrapper.sendRedirect(rootPath + "/temp.jsp");
                //	request.getRequestDispatcher(rootPath + "/temp.jsp").forward(req,res);
            }else {
                super.doFilter(req, res, chain);
            }
        }else if (url.contains("ueditor")) {
            System.out.println("使用自定义的过滤器");
            chain.doFilter(req, res);
        }else{
            System.out.println("使用默认的过滤器");
            super.doFilter(req, res, chain);
        }
    }
}
