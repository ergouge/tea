/**
 * 
 */
package swjtu.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * 数据库操作工具类
 * @author ERGOUGE
 * @email ergouge@gmail.com
 * 2014年9月6日 上午8:08:28
 */
public class DBConn {

	public static Connection getConn() {

		try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			// 创建连接

			Connection con = DriverManager.getConnection("jdbc:mysql://"+StaticParam.ip+":"+StaticParam.port+"/"+StaticParam.dBName+"?user="+StaticParam.dBUser+"&password="+StaticParam.dBPassword+"&useUnicode=true&amp;characterEncoding=UTF-8");


			return con;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static void closeConn(Connection conn){
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static void closeStatement(java.sql.PreparedStatement pst){
		if(pst != null){
			try {
				pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeResultSet(ResultSet res){
		if(res != null){
			try {
				res.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static boolean executeUpdate(String sql){
		//声明连接
		Connection conn = null;
		// 声明语句
		PreparedStatement pre = null;
		int resultNo=0;
		try {
			//
			conn = DBConn.getConn();
			//
			pre = conn.prepareStatement(sql);
			//
			resultNo=pre.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return resultNo>0?true:false;
	}
	
	public static int getToTalItemsNum(String sql){
		//声明数据库连接
		Connection conn = null;
		//声明实例
		PreparedStatement pre = null;
		
		//声明结果集
		java.sql.ResultSet rs = null;
		
		try{
			//数据库操作字符串
			//获取Connection连接
			conn = DBConn.getConn();
			//创建实例
			pre = conn.prepareStatement(sql);
			//设置相关数据项的关联
			//执行sql
			rs = pre.executeQuery();
			if(rs.next()){
					return rs.getInt(1);
				}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//关闭相关连接
			DBConn.closeResultSet(rs);
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return 0;
	}
	
	
	/** 
	* 根据构造的SQL语句，执行数据表记录的删除操作
	* @param 
	* @return 删除成功返回true，否则返回false 
	*/ 
	public static boolean deleteRec(String sql) {
		//声明数据库连接
		Connection conn = null;
		//申明实例
		PreparedStatement pre = null;
		try {
			//获得数据库连接
			conn = DBConn.getConn();
			//创建实例
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i >= 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//关闭连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}
}
