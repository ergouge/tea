package swjtu.util;

import java.util.Random;

public class RandomUtil
{
	// 设置最大数值
	public static final int INDEX = 9;

	/**
	* @Title: getRandomNum
	* @Description: 返回一个数字
	* @return int    返回类型
	* @throws
	*/
	public static int getRandomNum()
	{
		return new Random().nextInt(INDEX);
	}

	/**
	* @Title: getRandeomString
	* @Description: 按照指定数量返回相应的字符串
	* @return String    返回类型
	* @throws
	*/ 
	public static String getRandeomString(int size){
		StringBuffer str = new StringBuffer();
		for(int i=0;i<size;i++){
			str.append(getRandomNum());
		}
		return str.toString();
		
	}
}
