/**
 * created on 2014年9月19日
 * Author ergouge
 * Email ergouge@gmail.com
 * All rights reserved
 */
package swjtu.util;

/**
 * 此实体用于封装后台给前台发送的数据，以便于使用工具类将信息转成JSON
 * @author ERGOUGE
 * 2014年9月19日 上午9:27:52
 */
public class TipMessage {
	/**
	 * 结果，操作是否成功
	 */
	private boolean result;
	/**
	 * 信息，后台返回的信息
	 */
	private String msg;
	/**
	 * url地址，用户后台控制前台跳转
	 */
	private String url;
	
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}

