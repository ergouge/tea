package swjtu.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.opensymphony.xwork2.ActionContext;

public class StaticParam {
	public static int pageSize;

	public static String ip;
	public static String port;
	// 数据库名
	public static String dBName;
	// 数据库用户名
	public static String dBUser;
	// 数据库用户密码
	public static String dBPassword;

	static {
		Properties prop = new Properties();
		InputStream in = StaticParam.class.getClassLoader().getResourceAsStream("tea.properties");
		try {
			prop.load(in);
			// 从配置文件中得到数据库相关信息
			ip = prop.getProperty("ip").trim();
			port = prop.getProperty("port").trim();
			dBName = prop.getProperty("dBName").trim();
			dBUser = prop.getProperty("dBUser").trim();
			dBPassword = prop.getProperty("dBPassword").trim();
			pageSize = Integer.parseInt(prop.getProperty("pageSize").trim());
			// 设置全局变量
			ActionContext.getContext().getApplication()
					.put("globleSize", pageSize);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
