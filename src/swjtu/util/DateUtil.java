/**
 * created on 2014年10月12日
 * Author ergouge
 * Email ergouge@gmail.com
 * All rights reserved
 */
package swjtu.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TODO
 * @author ERGOUGE
 * 2014年10月12日 上午9:58:23
 */
public class DateUtil {
	/**
	 * 
	 * 获得当前时间字符串
	 * @author ERGOUGE
	 * 2014年10月12日 上午9:58:47
	 * @return
	 */
	public static String getCurrentDateString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");// 格式化日期，这样可以方便地修改日期格式
		java.util.Date now = new java.util.Date(); // 获取当前系统时间
		return dateFormat.format(now); // 将当前系统时间转换成字符串
		// Now = dateFormat.parse(dateFormat.format(Now));
	}
	
	public static String getCurrentDateString_2() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");// 格式化日期，这样可以方便地修改日期格式
		java.util.Date now = new java.util.Date(); // 获取当前系统时间
		return dateFormat.format(now); // 将当前系统时间转换成字符串
	}
	
	/**
	 * 
	 * 将传入的日期型数据转换为字符串型数据返回
	 * @author ERGOUGE
	 * 2014年10月12日 上午10:01:09
	 * @param date
	 * @return
	 */
	public static String convertDate2String(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(date);
	}
}
