package swjtu.util;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.struts2.ServletActionContext;

import com.google.gson.Gson;


/**
* @ClassName: JsonUtil
* @Description: Json工具类
* @author Gavin Nie 
* @E-mail GavinNieZW@163.com
* @date 2014年5月11日 下午6:14:22
*
*/
public class JSONUtil {

	/**
	 * 
	 * 将字符串类型的数据输出到前台
	 * @author ERGOUGE
	 * 2014年9月6日 下午10:18:11
	 * @param text
	 */
	public static void writeText(String text) {
		ServletActionContext.getResponse().setContentType(
				"text/html;charset=utf-8");
		try {
			PrintWriter out = ServletActionContext.getResponse().getWriter();
			out.write(text); // 写到前台
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 获得JSON字符串，将对象、列表转换成JSON字符串
	 * @author ERGOUGE
	 * 2014年9月6日 下午10:16:01
	 * @param object
	 * @return
	 */
	public static String getJsonString(Object object) {
		Gson gson = new Gson();
		String json = gson.toJson(object);
		return json;
	}
	
	/**
	 * 
	 * 输出到前台
	 * @author ERGOUGE
	 * 2014年9月6日 下午10:16:40
	 * @param object
	 */
	public static void writeJson(Object object) {
		String json = getJsonString(object);
		ServletActionContext.getResponse().setContentType(
				"text/html;charset=utf-8");
		try {
			PrintWriter out = ServletActionContext.getResponse().getWriter();
			out.write(json); // 写到前台
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
