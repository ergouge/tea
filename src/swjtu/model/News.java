package swjtu.model;
/**
 * 新闻实体
 * @author zhehua
 *
 */
public class News {
	/**
	 * 新闻id
	 */
	private int id;
	/**
	 * 新闻添加用户
	 */
	private User user;
	/**
	 * 所属菜单
	 */
	private Menu menu;
	/**
	 * 新闻标题
	 */
	private String news_title;
	/**
	 * 新闻内容
	 */
	private String news_content;
	/**
	 * 新闻浏览次数
	 */
	private int news_viewTimes;
	/**
	 * 新闻修改时间
	 */
	private String news_modifyTime;
	/**
	 * 新闻添加时间
	 */
	private String news_createTime;
	/**
	 * 新闻权重
	 */
	private int news_weight;
	/**
	 * 是否删除
	 */
	private int is_delete;
	/**
	 * 新闻备注
	 */
	private String news_remark;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getNews_title() {
		return news_title;
	}
	public void setNews_title(String news_title) {
		this.news_title = news_title;
	}
	public String getNews_content() {
		return news_content;
	}
	public void setNews_content(String news_content) {
		this.news_content = news_content;
	}
	public int getNews_viewTimes() {
		return news_viewTimes;
	}
	public void setNews_viewTimes(int news_viewTimes) {
		this.news_viewTimes = news_viewTimes;
	}
	public String getNews_modifyTime() {
		return news_modifyTime;
	}
	public void setNews_modifyTime(String news_modifyTime) {
		this.news_modifyTime = news_modifyTime;
	}
	public String getNews_createTime() {
		return news_createTime;
	}
	public void setNews_createTime(String news_createTime) {
		this.news_createTime = news_createTime;
	}
	public int getNews_weight() {
		return news_weight;
	}
	public void setNews_weight(int news_weight) {
		this.news_weight = news_weight;
	}
	public int getIs_delete() {
		return is_delete;
	}
	public void setIs_delete(int is_delete) {
		this.is_delete = is_delete;
	}
	public String getNews_remark() {
		return news_remark;
	}
	public void setNews_remark(String news_remark) {
		this.news_remark = news_remark;
	}
	
	
}
