package swjtu.model;
/**
 * 
* @ClassName: Shop
* @Description: 营销网络实体
* @author john
* @date 2015-1-20 下午8:45:15
*
 */
public class Shop {
	/**
	 * 递增id
	 */
	private int id;
	/**
	 * 营销类型名称
	 */
	private String shop_name;
	/**
	 * 网址
	 */
	private String shop_url;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getShop_name() {
		return shop_name;
	}
	
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	
	public String getShop_url() {
		return shop_url;
	}
	
	public void setShop_url(String shop_url) {
		this.shop_url = shop_url;
	}
	
	

}
