package swjtu.model;
/**
 * 产品实体
 * @author zhehua
 *
 */
public class Product {
	/**
	 * 产品id
	 */
	private int id;
	/**
	 * 添加产品用户
	 */
	private User user;
	/**
	 * 所属菜单
	 */
	private Menu menu;
	/**
	 * 产品名称
	 */
	private String pro_title;
	/**
	 * 产品介绍
	 */
	private String pro_intro;
	/**
	 * 产品原始价格
	 */
	private double pro_oriPrice;
	/**
	 * 产品最新价格
	 */
	private double pro_newPrice;
	/**
	 * 产品状态
	 */
	private String pro_status;
	/**
	 * 产品数量
	 */
	private int pro_num;
	/**
	 * 产品微信地址
	 */
	private String pro_weiURL;
	/**
	 * 产品淘宝地址
	 */
	private String pro_TaoBaoURL;
	/**
	 * 产品ICON地址
	 */
	private String pro_iconURL;
	/**
	 * 产品是否删除
	 */
	private int is_delete;
	/**
	 * 产品备注
	 */
	private String pro_remark;
	/**
	 * 产品类型
	 */
	private Type type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getPro_title() {
		return pro_title;
	}
	public void setPro_title(String pro_title) {
		this.pro_title = pro_title;
	}
	public String getPro_intro() {
		return pro_intro;
	}
	public void setPro_intro(String pro_intro) {
		this.pro_intro = pro_intro;
	}
	public double getPro_oriPrice() {
		return pro_oriPrice;
	}
	public void setPro_oriPrice(double pro_oriPrice) {
		this.pro_oriPrice = pro_oriPrice;
	}
	public double getPro_newPrice() {
		return pro_newPrice;
	}
	public void setPro_newPrice(double pro_newPrice) {
		this.pro_newPrice = pro_newPrice;
	}
	
	public String getPro_status() {
		return pro_status;
	}
	public void setPro_status(String proStatus) {
		pro_status = proStatus;
	}
	public int getPro_num() {
		return pro_num;
	}
	public void setPro_num(int pro_num) {
		this.pro_num = pro_num;
	}
	public String getPro_weiURL() {
		return pro_weiURL;
	}
	public void setPro_weiURL(String pro_weiURL) {
		this.pro_weiURL = pro_weiURL;
	}
	public String getPro_TaoBaoURL() {
		return pro_TaoBaoURL;
	}
	public void setPro_TaoBaoURL(String pro_TaoBaoURL) {
		this.pro_TaoBaoURL = pro_TaoBaoURL;
	}
	public String getPro_iconURL() {
		return pro_iconURL;
	}
	public void setPro_iconURL(String pro_iconURL) {
		this.pro_iconURL = pro_iconURL;
	}
	public int getIs_delete() {
		return is_delete;
	}
	public void setIs_delete(int is_delete) {
		this.is_delete = is_delete;
	}
	public String getPro_remark() {
		return pro_remark;
	}
	public void setPro_remark(String pro_remark) {
		this.pro_remark = pro_remark;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
}
