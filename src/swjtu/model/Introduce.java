package swjtu.model;
/**
 * 介绍实体
 * @author zhehua
 *
 */
public class Introduce {
	/**
	 * 介绍id
	 */
	private int id;
	/**
	 * 介绍生成时间
	 */
	private String intro_createTime;
	/**
	 * 介绍标题
	 */
	private String intro_title;
	/**
	 * 介绍内容
	 */
	private String intro_content;
	/**
	 * 是否可以删除
	 */
	private int is_delete;
	/**
	 * 浏览次数
	 */
	private int intro_viewTimes;
	/**
	 * 介绍添加人员
	 */
	private User user;
	/**
	 * 所属菜单
	 */
	private Menu menu;
	/**
	 * 介绍备注
	 */
	private String intro_remark;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIs_delete() {
		return is_delete;
	}
	public void setIs_delete(int is_delete) {
		this.is_delete = is_delete;
	}
	public int getIntro_viewTimes() {
		return intro_viewTimes;
	}
	public void setIntro_viewTimes(int intro_viewTimes) {
		this.intro_viewTimes = intro_viewTimes;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getIntro_createTime() {
		return intro_createTime;
	}
	public void setIntro_createTime(String intro_createTime) {
		this.intro_createTime = intro_createTime;
	}
	public String getIntro_title() {
		return intro_title;
	}
	public void setIntro_title(String intro_title) {
		this.intro_title = intro_title;
	}
	public String getIntro_content() {
		return intro_content;
	}
	public void setIntro_content(String intro_content) {
		this.intro_content = intro_content;
	}
	public String getIntro_remark() {
		return intro_remark;
	}
	public void setIntro_remark(String intro_remark) {
		this.intro_remark = intro_remark;
	}
	
	
	
	
}
