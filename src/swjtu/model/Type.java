package swjtu.model;
/**
 * 类型实体
 * @author zhehua
 *
 */
public class Type {
	/**
	 * 类型id
	 */
	private int id;
	/**
	 * 类型名称
	 */
	private String type_name;
	/**
	 * 类型备注
	 */
	private String type_remark;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType_name() {
		return type_name;
	}
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	public String getType_remark() {
		return type_remark;
	}
	public void setType_remark(String type_remark) {
		this.type_remark = type_remark;
	}
	
	
}
