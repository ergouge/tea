package swjtu.model;
/**
 * 用户实体
 * @author zhehua
 *
 */
public class User {
	/**
	 * 用户id
	 */
	private int id;
	/**
	 * 用户登录姓名
	 */
	private String login_name;
	/**
	 * 用户姓名
	 */
	private String user_name;
	/**
	 * 用户密码
	 */
	private String user_pwd;
	/**
	 * 用户年龄
	 */
	private int user_age;
	/**
	 * 用户性别
	 */
	private String user_gender;
	/**
	 * 用户邮箱
	 */
	private String user_email;
	/**
	 * 用户手机
	 */
	private String user_phone;
	/**
	 * 用户角色
	 */
	private String user_role;
	/**
	 * 用户地址
	 */
	private String user_adress;
	/**
	 * 是否删除
	 */
	private int is_delete;
	/**
	 * 用户备注
	 */
	private String user_remark;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_name() {
		return login_name;
	}
	public void setLogin_name(String login_name) {
		this.login_name = login_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public int getUser_age() {
		return user_age;
	}
	public void setUser_age(int user_age) {
		this.user_age = user_age;
	}
	public String getUser_gender() {
		return user_gender;
	}
	public void setUser_gender(String user_gender) {
		this.user_gender = user_gender;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}
	public String getUser_adress() {
		return user_adress;
	}
	public void setUser_adress(String user_adress) {
		this.user_adress = user_adress;
	}
	public int getIs_delete() {
		return is_delete;
	}
	public void setIs_delete(int is_delete) {
		this.is_delete = is_delete;
	}
	public String getUser_remark() {
		return user_remark;
	}
	public void setUser_remark(String user_remark) {
		this.user_remark = user_remark;
	}

}
