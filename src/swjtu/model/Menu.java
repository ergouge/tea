package swjtu.model;
/**
 * 菜单实体
 * @author zhehua
 *
 */
public class Menu {
	/**
	 * 菜单id
	 */
	private int id;
	/**
	 * 菜单名称
	 */
	private String menu_name;
	/**
	 * 菜单类型
	 */
	private String menu_type;
	/**
	 * 菜单备注
	 */
	private String menu_remark;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	public String getMenu_type() {
		return menu_type;
	}
	public void setMenu_type(String menu_type) {
		this.menu_type = menu_type;
	}
	public String getMenu_remark() {
		return menu_remark;
	}
	public void setMenu_remark(String menu_remark) {
		this.menu_remark = menu_remark;
	}
	
	
}
