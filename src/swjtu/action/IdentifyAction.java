package swjtu.action;

import java.io.ByteArrayInputStream;
import java.util.Map;

import swjtu.util.IdentifyCodeUtil;
import swjtu.util.JSONUtil;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * 生成验证码action
 * @author ERGOUGE
 * 2014年11月10日 下午1:37:33
 */
public class IdentifyAction extends ActionSupport
{
	private static final long serialVersionUID = 1L;
	private ByteArrayInputStream inputStream;
	private String identifyCode;
	private String indentifyCode;//检验验证码
	
	/**
	 * 
	 * 产生验证码
	 * @author ERGOUGE
	 * 2014年11月10日 下午3:07:07
	 * @return
	 * @throws Exception
	 */
	public String generateIdCode() throws Exception {
		IdentifyCodeUtil image = new IdentifyCodeUtil();
		this.setInputStream(image.generteCode());// 取得带有随机字符串的图片

		// 设置到session中
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put("code", image.getCodeStr());

		return SUCCESS;
	}

	
	/**
	 * 
	 * 检验验证码是否正确
	 * @author ERGOUGE
	 * 2014年11月10日 下午3:05:57
	 */
	@SuppressWarnings("null")
	public void validateIdCode() {
		if (indentifyCode == null && "".equals(indentifyCode)) {
			System.out.println("值不能为空");
			return;
		}

		// 获取session中存储的code值
		Map<String, Object> session = ActionContext.getContext().getSession();
		String sessionCode = (String) session.get("code");

		if (sessionCode == null && sessionCode.equals("")) {
			System.out.println("值不能为空");
			return;
		}

		// 判断两个值是否相等
		if (indentifyCode.equals(sessionCode)) {
			JSONUtil.writeText("true");
		} else {
			JSONUtil.writeText("false");
		}
	}
	
	
	
	public ByteArrayInputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(ByteArrayInputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getIdentifyCode() {
		return identifyCode;
	}

	public void setIdentifyCode(String identifyCode) {
		this.identifyCode = identifyCode;
	}

	public String getIndentifyCode() {
		return indentifyCode;
	}

	public void setIndentifyCode(String indentifyCode) {
		this.indentifyCode = indentifyCode;
	}
}
