package swjtu.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import swjtu.dao.BaseDao;
import swjtu.dao.UserDaoImpl;
import swjtu.model.User;
import swjtu.util.JSONUtil;
import swjtu.util.MD5Util;
import swjtu.util.TipMessage;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private BaseDao<User> baseDao;

	public UserAction() {
		super();
		baseDao = new UserDaoImpl();
	}
	
	public String login() {
		TipMessage tm = new TipMessage();
		try {
			if (user == null) {
				tm.setResult(false);
				tm.setMsg("错误");
				JSONUtil.writeJson(tm);
				return null;
			}
			user.setUser_pwd(MD5Util.MD5(user.getUser_pwd()));
			HttpServletRequest request = ServletActionContext.getRequest();
			/**
			 * 根据登陆名称找到用户对象
			 */
			String sql = "SELECT * from t_user WHERE login_name = '"
					+ user.getLogin_name() + "' and is_delete = 0";
			User userinfo = baseDao.findObjectBySQL(sql);

			// 跳转
			if (userinfo != null) {
				if (userinfo.getUser_pwd().equals(user.getUser_pwd())) {
					// 设置session
					ActionContext.getContext().getSession().put("userSession", userinfo);
					tm.setResult(true);
					tm.setMsg("登录成功");
					tm.setUrl("admin/index.jsp");
					JSONUtil.writeJson(tm);
					return null;
				} else {
					tm.setUrl("login.jsp");
					tm.setResult(false);
					tm.setMsg("用户名密码不一致");
					JSONUtil.writeJson(tm);
					return null;
				}
			} else {
				tm.setUrl("login.jsp");
				tm.setResult(false);
				tm.setMsg("没有此用户名");
				JSONUtil.writeJson(tm);
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	/**
	 * 退出
	 * 
	 */
	public String exit() {
		ActionContext.getContext().getSession().clear();
		return "Exit";
	}
	
	/**
	 * 根据id找到用户信息
	 * @return
	 */
	public String findUserById(){
		try {
			HttpServletRequest request =ServletActionContext.getRequest();
			User userInfo = baseDao.findObjectById(user);
			if(userInfo == null){
				
				return "FINDUSERERROR";
			}else{
				request.setAttribute("userInfo", userInfo);
				return "FINDUSERSUCCESS";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 修改用户信息
	 * @return
	 */
public void updateUserById(){
		try {
			User userInfo = baseDao.findObjectById(user);
			userInfo.setUser_adress(user.getUser_adress());
			userInfo.setUser_email(user.getUser_email());
			userInfo.setUser_gender(user.getUser_gender());
			userInfo.setUser_name(user.getUser_name());
			userInfo.setUser_age(user.getUser_age());
			userInfo.setUser_phone(user.getUser_phone());
			userInfo.setUser_pwd(MD5Util.MD5(user.getUser_pwd()));
			TipMessage tm = new TipMessage();
			if(baseDao.updateObject(userInfo)){
					tm.setMsg("用户更新成功");
					tm.setResult(true);
					tm.setUrl("");
					JSONUtil.writeJson(tm);
				}else {
					tm.setMsg("用户更新失败");
					tm.setResult(false);
					tm.setUrl("");
					JSONUtil.writeJson(tm);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	 

}
