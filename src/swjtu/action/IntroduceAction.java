package swjtu.action;

import swjtu.dao.BaseDao;
import swjtu.dao.IntroduceDaoImpl;
import swjtu.model.Introduce;
import swjtu.model.Menu;
import swjtu.util.JSONUtil;
import swjtu.util.TipMessage;

import com.opensymphony.xwork2.ActionSupport;

public class IntroduceAction extends ActionSupport{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private Introduce introduce;
	private Menu menu;
	private BaseDao<Introduce> baseDao;

	public IntroduceAction() {
		super();
		baseDao = new IntroduceDaoImpl();
	}
	
	/**
	 * 
	 * 获取一个对象
	 * @author ergouge
	 * 2015年1月13日 下午3:07:26
	 */
	public void getOneObj() {
		//浏览次数+1
		//System.out.println(introduce.getId());
		String sql2 = "update t_intro n set intro_viewTimes = intro_viewTimes+1 where n.menu_id = " +  menu.getId();
		boolean flag = baseDao.update(sql2);
		//获取对象
		String sql = " select * from t_intro n where is_delete = 0 and  n.menu_id = "+ menu.getId();
		
		Introduce tmp = baseDao.findObjectBySQL(sql);
		JSONUtil.writeJson(tmp);
	}
	
	/**
	 * 
	 * 修改企业简介
	 * @author ergouge
	 * 2015年1月13日 下午4:47:20
	 */
	public void updateObj() {
		String sql = " select * from t_intro n where is_delete = 0 and  n.id = "+ introduce.getId();
		Introduce tmp = baseDao.findObjectBySQL(sql);
		tmp.setIntro_content(introduce.getIntro_content());
		boolean f = baseDao.updateObject(tmp);
		TipMessage tm = new TipMessage();
		tm.setUrl("");
		if(f) {
			tm.setMsg("操作成功");
			tm.setResult(true);
		}else {
			tm.setMsg("操作失败");
			tm.setResult(false);
		}
		JSONUtil.writeJson(tm);
	}

	public Introduce getIntroduce() {
		return introduce;
	}

	public void setIntroduce(Introduce introduce) {
		this.introduce = introduce;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
}
