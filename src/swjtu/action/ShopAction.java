package swjtu.action;

import java.util.ArrayList;

import swjtu.dao.BaseDao;
import swjtu.dao.ShopDaoImp;
import swjtu.model.News;
import swjtu.model.Shop;
import swjtu.util.JSONUtil;
import swjtu.util.TipMessage;

import com.opensymphony.xwork2.ActionSupport;

public class ShopAction extends ActionSupport {
	private Shop shop;
	private BaseDao<Shop> baseDao;
	private int shopId;

	public ShopAction(){
		super();
		baseDao = new ShopDaoImp();
	}
	
	public void findShopById(){
		try {
			String sql = "select * from t_shop where id="+shopId;
			Shop shop = baseDao.findObjectBySQL(sql);		
			JSONUtil.writeJson(shop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void updateShop(){
		Shop tem = new Shop();
		tem = baseDao.findObjectById(shop);
		tem.setShop_url(shop.getShop_url());
		boolean mark = baseDao.updateObject(tem);
		TipMessage tm = new TipMessage();
		if(mark) {
			tm.setMsg("修改操作成功");
			tm.setResult(true);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}else {
			tm.setMsg("修改操作失败");
			tm.setResult(false);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}
		
		
	}
	
	public void findUrl(){
		try {
			String sql = "select * from t_shop";
			ArrayList<Shop> shops = (ArrayList<Shop>) baseDao.findObjectListBySQL(sql);
			JSONUtil.writeJson(shops);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	
	
	
	

}
