package swjtu.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import swjtu.dao.BaseDao;
import swjtu.dao.MenuDaoImpl;
import swjtu.dao.ProductDaoImpl;
import swjtu.dao.TypeDaoImpl;
import swjtu.model.Menu;
import swjtu.model.Product;
import swjtu.model.Type;
import swjtu.model.User;
import swjtu.util.DateUtil;
import swjtu.util.JSONUtil;
import swjtu.util.TipMessage;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ProductAction extends ActionSupport{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private Product product;
	private Type productType;
	private BaseDao<Product> baseDao;
	private BaseDao<Menu> menuDao; 
	private BaseDao<Type> typeDao;
	private User user;
	private Menu menu;
	private String ids;
	private int limit;
	private int start;
	private File productImg;
	public ProductAction() {
		super();
		baseDao = new ProductDaoImpl();
		menuDao = new MenuDaoImpl();
		typeDao = new TypeDaoImpl();
	}
	/**
	 * 根据id查找产品
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void findProductById(){
		try {
			Product oneProduct = baseDao.findObjectById(product);		
			JSONUtil.writeJson(oneProduct);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public String findProductFontById(){
		try {
			HttpServletRequest request = ServletActionContext.getRequest();
			if(product==null){
				return "FINDERROR";
			}
			Product oneProduct = baseDao.findObjectById(product);
			if(oneProduct!=null){
				request.setAttribute("product", oneProduct);
				return "FINDSUCCESS";
			}else{
				return "FINDERROR";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	/**
	 * 分页查找产品
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void getProductList(){
		String sql = "SELECT * FROM t_product p where 1=1 and p.is_delete=0";
		String sql1 = "select * from t_product p where 1=1 and p.is_delete=0 "; //ORDER BY id DESC  LIMIT "+limit+" OFFSET "+start;
		if(productType != null) {
			sql += " and p.type_id = " + productType.getId();
			sql1 += " and p.type_id = " + productType.getId();
		}
		sql1 = sql1 + " ORDER BY id DESC  LIMIT "+limit+" OFFSET "+start;
		ArrayList<Product> productList = (ArrayList<Product>) baseDao.findObjectListBySQL(sql1);
		int count = ((ArrayList<Product>) baseDao.findObjectListBySQL(sql)).size();
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("total", count);
		hashMap.put("rows", productList);
		JSONUtil.writeJson(hashMap);
	}
	/**
	 * 查找产品类型
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void findProductType(){
		String sql = "SELECT * FROM t_type";
		ArrayList<Type> typeList = (ArrayList<Type>) typeDao.findObjectListBySQL(sql);
		JSONUtil.writeJson(typeList);
		
	}
	/**
	 * 添加新产品
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void addProduct(){
    	String path = "asset/images/product/";
    	String realPath = ServletActionContext.getServletContext().getRealPath(
		"/asset/images/product/");
    	String imgName=DateUtil.getCurrentDateString_2() + ".jpg";
		
		User user = (User) ActionContext.getContext().getSession().get("userSession");
		product.setUser(user);		
		product.setPro_remark(DateUtil.convertDate2String(new Date()));
		menu = menuDao.findObjectById(menu);
		productType = typeDao.findObjectById(productType);
		product.setMenu(menu);
		product.setType(productType);		
		product.setIs_delete(0);
		product.setPro_iconURL(path.concat(imgName));
		// 写到指定的路径中
		File file = new File(realPath);
		// 如果指定的路径没有就创建
		if (!file.exists()) {
			file.mkdirs();
		}
		try {
			// list集合通过get(i)的方式来获取索引
			FileUtils.copyFile(getProductImg(), new File(file, imgName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean tmp = baseDao.saveObject(product);
		TipMessage tm = new TipMessage();
		if(tmp) {
			tm.setMsg("发布操作成功");
			tm.setResult(true);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}else {
			tm.setMsg("发布操作失败");
			tm.setResult(false);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}
	}
	/**
	 * 根据产品id删除产品
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void deleteProductById(){
		try {
			TipMessage tm = new TipMessage();
			String sql = "";
			sql = "update t_product t set t.is_delete=1 where id=" + product.getId();			
			if(baseDao.updateOrDeleteObjectBySQL(sql)){
				tm.setMsg("删除操作成功");
				tm.setResult(true);
				tm.setUrl("");
				JSONUtil.writeJson(tm);
			}else{
				tm.setMsg("删除操作成功");
				tm.setResult(false);
				tm.setUrl("");
				JSONUtil.writeJson(tm);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    /**
     * 批量删除产品
     * 
     * @date 2015-1-17
     * @returnType void
     * @author awen
     */
	public void deleteProduct() {
		String idstr[] = ids.split(",");
		String sql = "";
		TipMessage tm = new TipMessage();
		String msg = "";
		boolean flag = true;
		for (String string : idstr) {
			sql = "update t_product t set t.is_delete=1 where id=" + string;
			if(!baseDao.updateOrDeleteObjectBySQL(sql)) {
				msg += string + ",";
				flag = false;
			}
		}
		if(flag) {
			tm.setMsg("删除操作成功");
			tm.setResult(true);
		} else {
			tm.setMsg(msg);
			tm.setResult(false);
		}
		JSONUtil.writeJson(tm);
	}
	/**
	 * 更新产品信息
	 * 
	 * @date 2015-1-17
	 * @returnType void
	 * @author awen
	 */
	public void updateProduct() {
		String path = "asset/images/product/";
		String realPath = ServletActionContext.getServletContext().getRealPath(
				"/asset/images/product/");
		String imgName=DateUtil.getCurrentDateString_2() + ".jpg";

		User user = (User) ActionContext.getContext().getSession().get("userSession");

		//通过id获得product对象
		Product tmp = baseDao.findObjectById(product);
		menu = menuDao.findObjectById(menu);
		productType = typeDao.findObjectById(productType);
		tmp.setMenu(menu);
		tmp.setType(productType);
		tmp.setPro_iconURL(path.concat(imgName));
		tmp.setPro_intro(product.getPro_intro());
		tmp.setPro_num(product.getPro_num());
		tmp.setPro_newPrice(product.getPro_newPrice());
		tmp.setPro_oriPrice(product.getPro_oriPrice());
		tmp.setPro_title(product.getPro_title());
		tmp.setPro_TaoBaoURL(product.getPro_TaoBaoURL());
		tmp.setPro_weiURL(product.getPro_weiURL());
		if (productImg != null) {
			// 写到指定的路径中
			File file = new File(realPath);
			// 如果指定的路径没有就创建
			if (!file.exists()) {
				file.mkdirs();
			}
			try {
				// list集合通过get(i)的方式来获取索引
				FileUtils.copyFile(getProductImg(), new File(file, imgName));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		boolean flag = baseDao.updateObject(tmp);
		TipMessage tm = new TipMessage();
		if(flag) {
			tm.setMsg("修改操作成功");
			tm.setResult(true);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}else {
			tm.setMsg("修改操作失败");
			tm.setResult(false);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}
	}
	
	/**
	 * 
	 * 获取所有类别信息
	 * @author ERGOUGE
	 * 2015年6月24日 下午1:21:21
	 */
	public void getProductTypes() {
		List<Type> list = new ArrayList<Type>();
		list = typeDao.findObjectListBySQL("select * from t_type");
		if(list != null) {
			JSONUtil.writeJson(list);
		}else {
			
		}
	}
	
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Type getProductType() {
		return productType;
	}
	public void setProductType(Type productType) {
		this.productType = productType;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public File getProductImg() {
		return productImg;
	}
	public void setProductImg(File productImg) {
		this.productImg = productImg;
	}
	
}
