package swjtu.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import swjtu.dao.BaseDao;
import swjtu.dao.IntroduceDaoImpl;
import swjtu.dao.MenuDaoImpl;
import swjtu.dao.NewsDaoImpl;
import swjtu.dao.ProductDaoImpl;
import swjtu.dao.TypeDaoImpl;
import swjtu.dao.UserDaoImpl;
import swjtu.model.Introduce;
import swjtu.model.Menu;
import swjtu.model.News;
import swjtu.model.Product;
import swjtu.model.Type;
import swjtu.model.User;
import swjtu.util.DateUtil;
import swjtu.util.JSONUtil;
import swjtu.util.TipMessage;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class NewsAction extends ActionSupport{
	private News news;
	private User user;
	private Menu menu;
	private int rows;
	private int page;
	private int limit;
	private int start;
	private int menu_id;
	private String ids;
	private BaseDao<News> baseDao;
    private BaseDao<Menu> menuDao;
    
	public NewsAction() {
		super();
		baseDao = new NewsDaoImpl();
		menuDao = new MenuDaoImpl();
	}
	/**
	 * 分页获得新闻列表
	 * 
	 * @date 2015-1-14
	 * @returnType void
	 * @author awen
	 */
	public void getNewsListByParmsAndPages() {
		User user = (User) ActionContext.getContext().getSession().get("userSession");
		String sql = "SELECT * FROM t_news t1 WHERE 1=1   ";
		if(menu.getId()!= 0){
			sql+=" and menu_id=" + menu.getId();
		}		
		
		//sql1用来查询条数
		String sql1 = sql;
		//sql用来分页查询
		sql += " ORDER BY news_weight DESC,id DESC LIMIT " + rows * (page - 1)
		+ "," + rows;
		
		ArrayList<News> news = (ArrayList<News>) baseDao.findObjectListBySQL(sql);
		
//		ArrayList<News> news = (ArrayList<News>) baseDao.findObjectListBySQL(sql);
		int count = ((ArrayList<News>) baseDao.findObjectListBySQL(sql1)).size();
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("total", count);
		hashMap.put("rows", news);
		JSONUtil.writeJson(hashMap);
	}
	/**
	 * 
	* @Title: findNewsById 
	* @Description: 根据id找到某条新闻
	* @author:  Liujiang     
	* @throws
	 */
	public void findNewsById(){
		try {
			News oneNews = baseDao.findObjectById(news);		
			JSONUtil.writeJson(oneNews);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * 
	* @Title: getFrontPageNewsList 
	* @Description: 获取首页新闻
	* @author:  Liujiang     
	* @throws
	 */
	
	public void getFrontPageNewsList(){
		String sql ="select * from t_news t1 where 1=1 and t1.is_delete=0 order by t1.news_createTime  limit 8 offset 0   ";
		ArrayList<News> news = (ArrayList<News>) baseDao.findObjectListBySQL(sql);
		JSONUtil.writeJson(news);
	}

	/**
	 * 获得所有新闻
	 * 
	 * @date 2015-1-14
	 * @returnType void
	 * @author awen
	 */
	public void getNewsList(){
		String sql = "SELECT * FROM t_news t1 ,t_user t2 WHERE t1.is_delete=0 and t1.user_id = t2.id ";
		if(news.getNews_title() != null && !"".equals(news.getNews_title())){
			sql+=" and news_title like '%"+news.getNews_title()+"%'";
		}
		if(news.getNews_content() != null && !"".equals(news.getNews_content())){
			sql+=" and news_content like '%"+news.getNews_content()+"%'";
		}
		if(news.getUser().getUser_name() != null && !"".equals(news.getUser().getUser_name())){
			sql+="  and   t2.user_name like '%"+news.getUser().getUser_name()+"%'";
		}
		if(menu.getId()!= 0){
			sql+=" and menu_id=" + menu.getId();
		}		
		//sql用来查询条数
		String sql1 = sql;
		//sql1用来分页查询
		sql1 += " ORDER BY news_weight DESC,t1.id DESC LIMIT "+limit+" OFFSET "+start;
		ArrayList<News> news = (ArrayList<News>) baseDao.findObjectListBySQL(sql1);
		int count = ((ArrayList<News>) baseDao.findObjectListBySQL(sql)).size();
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		hashMap.put("total", count);
		hashMap.put("rows", news);
		JSONUtil.writeJson(hashMap);
	}
    /**
     * 添加新闻
     * 
     * @date 2015-1-14
     * @returnType void
     * @author awen
     */
	public void addNews(){
		User user = (User) ActionContext.getContext().getSession().get("userSession");
		news.setUser(user);		
		news.setNews_createTime(DateUtil.convertDate2String(new Date()));
		menu = menuDao.findObjectById(menu);
		news.setMenu(menu);
		boolean tmp = baseDao.saveObject(news);
		TipMessage tm = new TipMessage();
		if(tmp) {
			tm.setMsg("发布操作成功");
			tm.setResult(true);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}else {
			tm.setMsg("发布操作失败");
			tm.setResult(false);
			tm.setUrl("");
			JSONUtil.writeJson(tm);
		}
	}
	/**
	 * 根据id删除新闻
	 */
	public void deleteNewsById(){
		try {
			TipMessage tm = new TipMessage();
			String sql = "update t_news set is_delete=1 where id=" + news.getId();
			if(baseDao.updateOrDeleteObjectBySQL(sql)){
				tm.setMsg("删除操作成功");
				tm.setResult(true);
				tm.setUrl("");
				JSONUtil.writeJson(tm);
			}else{
				tm.setMsg("删除操作成功");
				tm.setResult(false);
				tm.setUrl("");
				JSONUtil.writeJson(tm);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 删除新闻
	 * 
	 * @date 2015-1-14
	 * @returnType void
	 * @author awen
	 */
	public void deleteNews() {
		String idstr[] = ids.split(",");
		String sql = "";
		TipMessage tm = new TipMessage();
		String msg = "";
		boolean flag = true;
		for (String string : idstr) {
			sql = "update t_news set is_delete=1 where id=" + string;
			if(!baseDao.updateOrDeleteObjectBySQL(sql)) {
				msg += string + ",";
				flag = false;
			}
		}
		if(flag) {
			tm.setMsg("删除操作成功");
			tm.setResult(true);
		} else {
			tm.setMsg(msg);
			tm.setResult(false);
		}
		JSONUtil.writeJson(tm);
	}
	/**
	 * 更新新闻
	 * 
	 * @date 2015-1-14
	 * @returnType void
	 * @author awen
	 */
	public void updateNews() {
		//获得session值
		HttpServletRequest request =ServletActionContext.getRequest();
		User user = (User) ActionContext.getContext().getSession().get("userSession");
		news.setUser(user);
		menu = menuDao.findObjectById(menu);
		news.setMenu(menu);
	
		//执行更新操作
		boolean mark = baseDao.updateObject(news);
		TipMessage tm = new TipMessage();
		if(mark) {
			tm.setMsg("修改操作成功");
			tm.setResult(true);
			tm.setUrl("admin/newsManage/newsList.jsp");
			JSONUtil.writeJson(tm);
		}else {
			tm.setMsg("修改操作失败");
			tm.setResult(false);
			tm.setUrl("admin/newsManage/newsList.jsp");
			JSONUtil.writeJson(tm);
		}
	}

	/**
	 * 查看新闻
	 *
	 * @date 2015-1-20
	 * @returnType void
	 * @author ergouge
	 */
	public String viewNews() {
		HttpServletRequest request =ServletActionContext.getRequest();
		//先增加阅读次数
		String sql = "update t_news t set news_viewTimes=news_viewTimes+1 where t.id="+news.getId();
		baseDao.update(sql);
		News tmp = baseDao.findObjectById(news);
		if(tmp != null){
			request.setAttribute("news_detail", tmp);
			return "news_detail";
		}else {
			return "news_error";
		}
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(int menuId) {
		menu_id = menuId;
	}

	public BaseDao<News> getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(BaseDao<News> baseDao) {
		this.baseDao = baseDao;
	}

	
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}

	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}

}
