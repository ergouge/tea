package swjtu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.Menu;
import swjtu.model.Introduce;
import swjtu.model.User;
import swjtu.util.DBConn;

public class IntroduceDaoImpl implements BaseDao<Introduce>{

	@Override
	public Introduce findObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					if (rs.next()) {
						Introduce introduce = new Introduce();
						introduce = SetSqlRes(rs);
							return introduce;
						}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	
	@Override
	public ArrayList<Introduce> findObjectListBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				ArrayList<Introduce> introducees = new ArrayList<Introduce>();
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					while (rs.next()) {
						Introduce introduce = new Introduce();
						introduce = SetSqlRes(rs);
						introducees.add(introduce);
						}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return introducees;
	}

	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	@Override
	public Introduce findObjectById(Introduce introduce) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "";
					if (introduce != null) {
						sql = " select * from t_introduce as u where 1=1 "
									+ " and  u.id = "+introduce.getId();
						// 创建实例
						pre = conn.prepareStatement(sql);
						rs = pre.executeQuery();
						if (rs.next()) {
							Introduce introduceInfo = new Introduce();
							introduceInfo = SetSqlRes(rs);
							return introduceInfo;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	@Override
	public boolean saveObject(Introduce introduce) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		try {
			// 数据库操作字符串
			String sql = "INSERT INTO t_introduce (intro_createTime," +
					"intro_title,intro_content,is_delete,intro_viewTimes," +
					"user_id,menu_id,intro_remark) VALUES(?,?,?,?,?,?,?,?)";
			// 获取Connection连接
			conn = DBConn.getConn();
			// 创建实例
			pre = conn.prepareStatement(sql);
			// 设置相关数据项的关联
			// 调用SetPreItems函数，设置相关数据项的关联
			SetPreItems(pre,introduce);

			//添加
			// 返回更新数据库记录条数
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	private void SetPreItems(PreparedStatement pre, Introduce introduce) {
		try {
			pre.setString(1, introduce.getIntro_createTime());
			pre.setString(2, introduce.getIntro_title());
			pre.setString(3, introduce.getIntro_content());
			pre.setInt(4, introduce.getIs_delete());
			pre.setInt(5, introduce.getIntro_viewTimes());
			pre.setInt(6, introduce.getUser().getId());
			pre.setInt(7, introduce.getMenu().getId());
			pre.setString(8, introduce.getIntro_remark());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	@Override
	public boolean updateObject(Introduce introduce) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "update t_intro set intro_createTime=?," +
					"intro_title=?,intro_content=?,is_delete=?,intro_viewTimes=?," +
					"user_id=?,menu_id=?,intro_remark=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,introduce);
					pre.setInt(9, introduce.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean deleteObject(Introduce introduce) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "DELETE FROM t_introduce WHERE id = "+introduce.getId();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Introduce SetSqlRes(ResultSet rs) {
		Introduce tmp = new Introduce();
		try {
			tmp.setId(rs.getInt("id"));
			Menu menu = new Menu();
			menu.setId(rs.getInt("menu_id"));
			BaseDao dao = new MenuDaoImpl();
			tmp.setMenu((Menu) dao.findObjectById(menu));
			tmp.setIs_delete(rs.getInt("is_delete"));
			tmp.setIntro_content(rs.getString("intro_content"));
			tmp.setIntro_createTime(rs.getString("intro_createTime"));
			tmp.setIntro_remark(rs.getString("intro_remark"));
			tmp.setIntro_title(rs.getString("intro_title"));
			tmp.setIntro_viewTimes(rs.getInt("intro_viewTimes"));
			User user = new User();
			user.setId(rs.getInt("user_id"));
			 dao = new UserDaoImpl();
			tmp.setUser((User) dao.findObjectById(user));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tmp;
	}


	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}


}
