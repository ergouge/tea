package swjtu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.Shop;
import swjtu.util.DBConn;

public class ShopDaoImp implements BaseDao<Shop>{

	@Override
	public Shop findObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					if (rs.next()) {
						Shop shop = new Shop();
						shop = SetSqlRes(rs);
							return shop;
						}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	

	@Override
	public ArrayList<Shop> findObjectListBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				ArrayList<Shop> shops = new ArrayList<Shop>();
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					while (rs.next()) {
						Shop shop = new Shop();
						shop = SetSqlRes(rs);
						shops.add(shop);
						}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return shops;
	}

	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	@Override
	public Shop findObjectById(Shop shop) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "";
					if (shop != null) {
						sql = " select * from t_shop as u where 1=1 "
									+ " and  u.id = "+shop.getId();
						// 创建实例
						pre = conn.prepareStatement(sql);
						rs = pre.executeQuery();
						if (rs.next()) {
							Shop shopInfo = new Shop();
							shopInfo = SetSqlRes(rs);
							return shopInfo;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	@Override
	public boolean saveObject(Shop shop) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		try {
			// 数据库操作字符串
			String sql = "INSERT INTO t_shop(shop_name," +
					"shop_url) VALUES(?,?)";
			// 获取Connection连接
			conn = DBConn.getConn();
			// 创建实例
			pre = conn.prepareStatement(sql);
			// 设置相关数据项的关联
			// 调用SetPreItems函数，设置相关数据项的关联
			SetPreItems(pre,shop);

			//添加
			// 返回更新数据库记录条数
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}
	@Override
	public boolean updateObject(Shop shop) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "update t_shop set shop_name=?," +
					"shop_url=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,shop);
					pre.setInt(3, shop.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean deleteObject(Shop shop) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "DELETE FROM t_shop WHERE id = "+shop.getId();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}
	private Shop SetSqlRes(ResultSet rs) {
		Shop tmp = new Shop();
		try {
			tmp.setId(rs.getInt("id"));
			tmp.setShop_name(rs.getString("shop_name"));
			tmp.setShop_url(rs.getString("shop_url"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tmp;
	}
	
	private void SetPreItems(PreparedStatement pre, Shop shop) {
		try {
			pre.setString(1, shop.getShop_name());
			pre.setString(2, shop.getShop_url());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}
}
