package swjtu.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.Menu;
import swjtu.model.Product;
import swjtu.model.Type;
import swjtu.model.User;
import swjtu.util.DBConn;

public class ProductDaoImpl implements BaseDao<Product>{

	@Override
	public Product findObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					if (rs.next()) {
						Product product = new Product();
						product = SetSqlRes(rs);
							return product;
						}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}



	@Override
	public ArrayList<Product> findObjectListBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		java.sql.ResultSet rs = null;
		ArrayList<Product> products = new ArrayList<Product>();
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			rs = pre.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product = SetSqlRes(rs);
				products.add(product);
				}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeResultSet(rs);
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return products;
	}

	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public Product findObjectById(Product product) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		java.sql.ResultSet rs = null;
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			String sql = "";
			if (product != null) {
				sql = " select * from t_product as u where 1=1 "
							+ " and  u.id = "+product.getId();
				// 创建实例
				pre = conn.prepareStatement(sql);
				rs = pre.executeQuery();
				if (rs.next()) {
					Product productInfo = new Product();
					productInfo = SetSqlRes(rs);
					return productInfo;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeResultSet(rs);
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return null;
	}

	@Override
	public boolean saveObject(Product product) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "INSERT INTO t_product(user_id,menu_id,pro_title," +
							"pro_intro,pro_oriPrice,	pro_newPrice,pro_status,pro_num,pro_weiURL,pro_TaoBaoURL,pro_iconURL,is_delete,pro_remark,type_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 设置相关数据项的关联
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,product);

					//添加
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}


	@Override
	public boolean updateObject(Product product) {

		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					
					// 数据库操作字符串
					String sql = "update t_product set user_id=?," +
					"menu_id=?,pro_title=?,pro_intro=?,pro_oriPrice=?," +
					"pro_newPrice=?,pro_status=?,pro_num=?,pro_weiURL=?,pro_TaoBaoURL=?,pro_iconURL=?,is_delete=?,pro_remark=?,type_id=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,product);
					pre.setInt(15, product.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean deleteObject(Product product) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			String sql = "DELETE FROM t_product WHERE id = "+product.getId();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}

	
	private void SetPreItems(PreparedStatement pre, Product product) {
		try {
			pre.setInt(1, product.getUser().getId());
			pre.setInt(2, product.getMenu().getId());
			pre.setString(3, product.getPro_title());
			pre.setString(4, product.getPro_intro());
			pre.setDouble(5, product.getPro_oriPrice());
			pre.setDouble(6, product.getPro_newPrice());
			pre.setString(7, product.getPro_status());
			pre.setInt(8, product.getPro_num());
			pre.setString(9, product.getPro_weiURL());
			pre.setString(10, product.getPro_TaoBaoURL());
			pre.setString(11, product.getPro_iconURL());
			pre.setInt(12, product.getIs_delete());
			pre.setString(13, product.getPro_remark());
			pre.setInt(14, product.getType().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Product SetSqlRes(ResultSet rs) {
		Product product = new Product();
		try {
			product.setId(rs.getInt("id"));
			Menu menu = new Menu();
			menu.setId(rs.getInt("menu_id"));
			BaseDao dao = new MenuDaoImpl();
			product.setMenu((Menu)dao.findObjectById(menu));
			product.setIs_delete(rs.getInt("is_delete"));
			product.setPro_iconURL(rs.getString("pro_iconURL"));
			product.setPro_intro(rs.getString("pro_intro"));
			product.setPro_newPrice(rs.getDouble("pro_newPrice"));
			product.setPro_num(rs.getInt("pro_num"));
			product.setPro_oriPrice(rs.getDouble("pro_oriPrice"));
			product.setPro_remark(rs.getString("pro_remark"));
			product.setPro_status(rs.getString("pro_status"));
			product.setPro_TaoBaoURL(rs.getString("pro_TaoBaoURL"));
			product.setPro_title(rs.getString("pro_title"));
			product.setPro_weiURL(rs.getString("pro_weiURL"));
			Type type =new Type();
			type.setId(rs.getInt("type_id"));
			dao = new TypeDaoImpl();
			product.setType((Type)dao.findObjectById(type));
			User user =new User();
			user.setId(rs.getInt("user_id"));
			dao = new UserDaoImpl();
			product.setUser((User)dao.findObjectById(user));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;
	}
}
