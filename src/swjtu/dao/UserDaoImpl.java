package swjtu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.User;
import swjtu.util.DBConn;

public class UserDaoImpl implements BaseDao<User>{
	
	/**
	 * 根据sql返回用户对象
	 */
	@Override
	public User findObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		java.sql.ResultSet rs = null;
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			rs = pre.executeQuery();
			if (rs.next()) {
					User userInfo = new User();
					userInfo = SetSqlRes(rs);
					return userInfo;
				}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeResultSet(rs);
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return null;
	}
	
	/**
	 * 根据sql返回用户对象列表
	 */
	@Override
	public ArrayList<User> findObjectListBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				ArrayList<User> users = new ArrayList<User>();
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					while (rs.next()) {
							User userInfo = new User();
							userInfo = SetSqlRes(rs);
							users.add(userInfo);
						}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return users;
	}
	/**
	 * 根据sql更新或者删除对象信息
	 */
	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	/**
	 * 根据对象id返回用户对象信息
	 */

	@Override
	public User findObjectById(User user) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		java.sql.ResultSet rs = null;
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			String sql = "";
			if (user != null) {
				sql = " select * from t_user as u where 1=1 "
							+ " and  u.id = "+user.getId();
				// 创建实例
				pre = conn.prepareStatement(sql);
				rs = pre.executeQuery();
				if (rs.next()) {
					User userInfo = new User();
					userInfo = SetSqlRes(rs);
					return userInfo;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeResultSet(rs);
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return null;
	}
	/**
	 * 保存对象
	 */
	@Override
	public  boolean saveObject(User user) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		try {
			// 数据库操作字符串
			String sql = "INSERT INTO t_user(login_name,user_name,user_pwd,user_age," +
					"user_gender,user_email,user_phone,user_role,user_address,is_delete,user_remark) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			// 获取Connection连接
			conn = DBConn.getConn();
			// 创建实例
			pre = conn.prepareStatement(sql);
			// 设置相关数据项的关联
			// 调用SetPreItems函数，设置相关数据项的关联
			SetPreItems(pre,user);

			//添加
			// 返回更新数据库记录条数
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}
	/**
	 * 更新对象
	 */
	@Override
	public boolean updateObject(User user) {
				// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "update t_user set login_name=?,user_name=?,user_pwd=?,user_age=?," +
					"user_gender=?,user_email=?,user_phone=?,user_role=?,user_address=?,is_delete=?,user_remark=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,user);
					pre.setInt(12, user.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private User SetSqlRes(ResultSet rs) {
		User tmp = new User();
		try {
			tmp.setId(rs.getInt("id"));
			tmp.setIs_delete(rs.getInt("is_delete"));
			tmp.setLogin_name(rs.getString("login_name"));
			tmp.setUser_adress(rs.getString("user_address"));
			tmp.setUser_age(rs.getInt("user_age"));
			tmp.setUser_email(rs.getString("user_email"));
			tmp.setUser_gender(rs.getString("user_gender"));
			tmp.setUser_name(rs.getString("user_name"));
			tmp.setUser_phone(rs.getString("user_phone"));
			tmp.setUser_pwd(rs.getString("user_pwd"));
			tmp.setUser_remark(rs.getString("user_remark"));
			tmp.setUser_role(rs.getString("user_role"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tmp;
	}
	// 设置相关数据项的关联
	private void SetPreItems(PreparedStatement pre,User user)
		{
		try {
			pre.setString(1, user.getLogin_name());
			pre.setString(2, user.getUser_name());
			pre.setString(3, user.getUser_pwd());
			pre.setInt(4, user.getUser_age());
			pre.setString(5, user.getUser_gender());
			pre.setString(6, user.getUser_email());
			pre.setString(7, user.getUser_phone());
			pre.setString(8, user.getUser_role());
			pre.setString(9, user.getUser_adress());
			pre.setInt(10, user.getIs_delete());
			pre.setString(11, user.getUser_remark());
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		}

	@Override
	public boolean deleteObject(User user) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "DELETE FROM t_user WHERE id = "+user.getId();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}



	




}
