package swjtu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.Menu;
import swjtu.model.News;
import swjtu.model.User;
import swjtu.util.DBConn;

public class NewsDaoImpl implements BaseDao<News>{

	@Override
	public News findObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					if (rs.next()) {
						News news = new News();
						news = SetSqlRes(rs);
							return news;
						}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	
	@Override
	public ArrayList<News> findObjectListBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				ArrayList<News> newses = new ArrayList<News>();
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					while (rs.next()) {
						News news = new News();
						news = SetSqlRes(rs);
						newses.add(news);
						}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return newses;
	}

	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	@Override
	public News findObjectById(News news) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "";
					if (news != null) {
						sql = " select * from t_news as u where 1=1 "
									+ " and  u.id = "+news.getId();
						// 创建实例
						pre = conn.prepareStatement(sql);
						rs = pre.executeQuery();
						if (rs.next()) {
							News newsInfo = new News();
							newsInfo = SetSqlRes(rs);
							return newsInfo;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	@Override
	public boolean saveObject(News news) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		try {
			// 数据库操作字符串
			String sql = "INSERT INTO t_news (user_id," +
					"menu_id,news_title,news_content,news_viewTimes," +
					"news_modifyTime,news_createTime,news_weight,is_delete,news_remark) VALUES(?,?,?,?,?,?,?,?,?,?)";
			// 获取Connection连接
			conn = DBConn.getConn();
			// 创建实例
			pre = conn.prepareStatement(sql);
			// 设置相关数据项的关联
			// 调用SetPreItems函数，设置相关数据项的关联
			SetPreItems(pre,news);

			//添加
			// 返回更新数据库记录条数
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	private void SetPreItems(PreparedStatement pre, News news) {
		try {
			pre.setInt(1, news.getUser().getId());
			pre.setInt(2, news.getMenu().getId());
			pre.setString(3, news.getNews_title());
			pre.setString(4, news.getNews_content());
			pre.setInt(5, news.getNews_viewTimes());
			pre.setString(6, news.getNews_modifyTime());
			pre.setString(7, news.getNews_createTime());
			pre.setInt(8, news.getNews_weight());
			pre.setInt(9, news.getIs_delete());
			pre.setString(10, news.getNews_remark());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


	@Override
	public boolean updateObject(News news) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "update t_news set user_id=?," +
					"menu_id=?,news_title=?,news_content=?,news_viewTimes=?," +
					"news_modifyTime=?,news_createTime=?,news_weight=?,is_delete=?,news_remark=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,news);
					pre.setInt(11, news.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean deleteObject(News news) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "DELETE FROM t_news WHERE id = "+news.getId();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private News SetSqlRes(ResultSet rs) {
		News tmp = new News();
		try {
			tmp.setId(rs.getInt("id"));
			Menu menu = new Menu();
			menu.setId(rs.getInt("menu_id"));
			BaseDao dao = new MenuDaoImpl();
			tmp.setMenu((Menu) dao.findObjectById(menu));
			tmp.setIs_delete(rs.getInt("is_delete"));
			tmp.setNews_content(rs.getString("news_content"));
			tmp.setNews_createTime(rs.getString("news_createTime"));
			tmp.setNews_modifyTime(rs.getString("news_modifyTime"));
			tmp.setNews_remark(rs.getString("news_remark"));
			tmp.setNews_title(rs.getString("news_title"));
			tmp.setNews_viewTimes(rs.getInt("news_viewTimes"));
			tmp.setNews_weight(rs.getInt("news_weight"));
			User user = new User();
			user.setId(rs.getInt("user_id"));
			 dao = new UserDaoImpl();
			tmp.setUser((User) dao.findObjectById(user));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tmp;
	}


	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}


}
