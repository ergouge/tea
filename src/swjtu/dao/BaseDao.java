package swjtu.dao;

import java.util.ArrayList;


public interface BaseDao<T>{
	/**
	 * 根据sql返回对象
	 */
	public T findObjectBySQL(String sql);
	/**
	 * 根据sql返回对象列表
	 */
	public ArrayList<T> findObjectListBySQL(String sql);
	/**
	 * 根据sql更新或者删除对象信息
	 */
	public  boolean updateOrDeleteObjectBySQL(String sql);
	/**
	 * 根据对象id返回对象信息
	 */
	public  T findObjectById(T object);
	/**
	 * 插入对象
	 */
	public  boolean saveObject(T object);
	/**
	 * 更新对象信息
	 */
	public boolean updateObject(T object);
	/**
	 * 销毁对象信息
	 */
	public boolean deleteObject(T object);
	
	/**
	 * 
	* @Title: update
	* @Description: 处理一些不具有共性的操作
	* @param @param sql
	* @param @return    设定文件
	* @return boolean    返回类型
	* @author john  
	* @throws
	 */
	public boolean update(String sql);
}
