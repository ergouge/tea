package swjtu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import swjtu.model.Menu;
import swjtu.util.DBConn;

public class MenuDaoImpl implements BaseDao<Menu>{

	@Override
	public Menu findObjectBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					if (rs.next()) {
						Menu menu = new Menu();
						menu = SetSqlRes(rs);
							return menu;
						}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	

	@Override
	public ArrayList<Menu> findObjectListBySQL(String sql) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				ArrayList<Menu> menus = new ArrayList<Menu>();
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					pre = conn.prepareStatement(sql);
					rs = pre.executeQuery();
					while (rs.next()) {
						Menu menu = new Menu();
						menu = SetSqlRes(rs);
						menus.add(menu);
						}
					
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return menus;
	}

	@Override
	public boolean updateOrDeleteObjectBySQL(String sql) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		// 声明结果集
		try{
			// 获取Connection连接
			conn = DBConn.getConn();
			pre = conn.prepareStatement(sql);
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}

	@Override
	public Menu findObjectById(Menu menu) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				java.sql.ResultSet rs = null;
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "";
					if (menu != null) {
						sql = " select * from t_menu as u where 1=1 "
									+ " and  u.id = "+menu.getId();
						// 创建实例
						pre = conn.prepareStatement(sql);
						rs = pre.executeQuery();
						if (rs.next()) {
							Menu menuInfo = new Menu();
							menuInfo = SetSqlRes(rs);
							return menuInfo;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeResultSet(rs);
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return null;
	}

	@Override
	public boolean saveObject(Menu menu) {
		// 声明数据库连接
		Connection conn = null;
		// 声明实例
		PreparedStatement pre = null;
		try {
			// 数据库操作字符串
			String sql = "INSERT INTO t_menu(menu_name," +
					"menu_type,menu_remark) VALUES(?,?,?)";
			// 获取Connection连接
			conn = DBConn.getConn();
			// 创建实例
			pre = conn.prepareStatement(sql);
			// 设置相关数据项的关联
			// 调用SetPreItems函数，设置相关数据项的关联
			SetPreItems(pre,menu);

			//添加
			// 返回更新数据库记录条数
			int i = pre.executeUpdate();
			if (i > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭相关连接
			DBConn.closeStatement(pre);
			DBConn.closeConn(conn);
		}
		return false;
	}
	@Override
	public boolean updateObject(Menu menu) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				try {
					// 数据库操作字符串
					String sql = "update t_menu set menu_name=?," +
					"menu_type=?,menu_remark=? where id =?";
					// 获取Connection连接
					conn = DBConn.getConn();
					// 创建实例
					pre = conn.prepareStatement(sql);
					// 调用SetPreItems函数，设置相关数据项的关联
					SetPreItems(pre,menu);
					pre.setInt(4, menu.getId());
					// 返回更新数据库记录条数
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}

	@Override
	public boolean deleteObject(Menu menu) {
		// 声明数据库连接
				Connection conn = null;
				// 声明实例
				PreparedStatement pre = null;
				// 声明结果集
				try{
					// 获取Connection连接
					conn = DBConn.getConn();
					String sql = "DELETE FROM t_menu WHERE id = "+menu.getId();
					pre = conn.prepareStatement(sql);
					int i = pre.executeUpdate();
					if (i > 0) {
						return true;
					}else{
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// 关闭相关连接
					DBConn.closeStatement(pre);
					DBConn.closeConn(conn);
				}
				return false;
	}
	private Menu SetSqlRes(ResultSet rs) {
		Menu tmp = new Menu();
		try {
			tmp.setId(rs.getInt("id"));
			tmp.setMenu_remark(rs.getString("menu_remark"));
			tmp.setMenu_name(rs.getString("menu_name"));
			tmp.setMenu_type(rs.getString("menu_type"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tmp;
	}
	
	private void SetPreItems(PreparedStatement pre, Menu menu) {
		try {
			pre.setString(1, menu.getMenu_name());
			pre.setString(2, menu.getMenu_type());
			pre.setString(3,menu.getMenu_remark());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public boolean update(String sql) {
		return DBConn.executeUpdate(sql);
	}
}
